<?php
/**
 * @package		OpenCart
 * @author		Daniel Kerr
 * @copyright	Copyright (c) 2005 - 2017, OpenCart, Ltd. (https://www.opencart.com/)
 * @license		https://opensource.org/licenses/GPL-3.0
 * @link		https://www.opencart.com
*/

/**
* Encryption class
*/
final class Encryption {
	/**
     * 
     *
     * @param	string	$key
	 * @param	string	$value
	 * 
	 * @return	string
     */	
	public function encrypt($key, $value) {
		// Remove the base64 encoding from our key
		$encryption_key = base64_decode($value);
		
		// Generate an initialization vector		
		$iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length('aes-256-gcm'));
		
		// Encrypt the data using AES 256 encryption in GCM mode using our encryption key and initialization vector.		
		$encrypted = openssl_encrypt($key, 'aes-256-gcm', $encryption_key, 0, $iv);
		
		// The $iv is just as important as the key for decrypting, so save it with our encrypted data using a unique separator (::)		
		return base64_encode($encrypted . '::' . $iv);
	}
	
	/**
     * 
     *
     * @param	string	$key
	 * @param	string	$value
	 * 
	 * @return	string
     */
	public function decrypt($key, $value) {
		// Remove the base64 encoding from our key
		$encryption_key = base64_decode($value);
		
		// To decrypt, split the encrypted data from our IV - our unique separator used was "::"
		list($encrypted_data, $iv) = explode('::', base64_decode($key), 2);
		
		return openssl_decrypt($encrypted_data, 'aes-256-gcm', $encryption_key, 0, $iv);
	}
}
