<!DOCTYPE html>
<html lang="ru">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,700" rel="stylesheet">
  <!-- <link href="catalog/view/theme/themeNew/stylesheet/stylesheet.css" rel="stylesheet"> -->
  <link href="catalog/view/theme/themeNew/stylesheet/app.css" rel="stylesheet">
  <title><?php echo $title; ?></title>
  <base href="<?php echo $base; ?>" />
  <?php if ($description) { ?>
  <meta name="description" content="<?php echo $description; ?>" />
  <?php } ?>
  <?php if ($keywords) { ?>
  <meta name="keywords" content= "<?php echo $keywords; ?>" />
  <?php } ?>
  <?php foreach ($styles as $style) { ?>
  <link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
  <?php } ?>
  
  <?php foreach ($links as $link) { ?>
  <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
  <?php } ?>
  <?php foreach ($scripts as $script) { ?>
   <!-- <script src="<?php echo $script; ?>" type="text/javascript"></script> -->
  <?php } ?>
  <?php foreach ($analytics as $analytic) { ?>
  <?php echo $analytic; ?>
  <?php } ?>
</head>
<body class="<?php echo $class; ?>">
  <div class="page-wrap">
    <header class="header">
      <div class="header-auth">
        <div class="header-auth_content">
          <div class="header-mobile_contact-block">
            <a href="" class="header-contact_link">Контакты</a>
          </div>
          <div class="header-auth_block">
            <?php if ($logged) { ?>
              <a href="<?php echo $account; ?>" class="header-auth_link">Аккаунт</a>
              <a href="<?php echo $logout; ?>" class="header-auth_link">Выйти</a>
            <?php } else { ?>
              <a href="<?php echo $register; ?>" class="header-auth_link">Вход</a>
              <a href="<?php echo $login; ?>" class="header-auth_link">Регистрация</a>
            <?php } ?>
          </div>
        </div>
      </div>
      <div class="header_content">
        <div class="header_content_wrap">
          <div class="header-logo_info-block">
            <div class="header-logo_img-block animated fadeInUp delay-1s">
              <?php if ($logo) { ?>
                <img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="header-logo_img" />
              <?php } else { ?>
                <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
              <?php } ?>
            </div>
            <div class="header-logo_img-title animated fadeInUp delay-1s">
              <p class="header-logo_title">
                Интернет-магазин светодиодной продукции
              </p>
            </div>
          </div>
          <div class="header-desktop">
            <div class="header-desktop_city-phone animated fadeInUp delay-1s">
              <div class="header-city_block">
                <p class="header-city_name  header-city_name__desktop">г. Воронеж</p>
              </div>
              <div class="header-phone_block">
                <a href="" class="header-phone_link header-phone_link__desktop"><?php echo $telephone; ?></a>
              </div>
            </div>
            <div class="header-desktop_city-phone animated fadeInUp delay-1s">
              <div class="header-city_block">
                <p class="header-city_name  header-city_name__desktop">г. Воронеж</p>
              </div>
              <div class="header-phone_block">
                <a href="" class="header-phone_link header-phone_link__desktop"><?php echo $telephone; ?></a>
                <a href="" class="header-phone_link header-phone_link__desktop"><?php echo $telephone; ?></a>
              </div>
            </div>
          </div>
          <div class="header-interaction_block">
            <div class="header-upload_block animated fadeInUp delay-1s">
              <a href="" class="header-upload_link" data-project-form-js>загрузить проект</a>
            </div>
            <div class="header-interaction_basket-block animated fadeInUp delay-1s">
              <?php echo $cart; ?>
            </div>
          </div>
          <div class="header-mobile-toggle" data-toggle-js>
            <span></span>
          </div>
        </div>
      </div>
      <div class="header_content header_content__nav">
        <div class="header-desktop_fixed-logo">
          <?php if ($logo) { ?>
            <img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" /></a>
          <?php } else { ?>
            <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
          <?php } ?>
        </div>
        <div class="header_content_wrap header_content_wrap__nav">
          <?php if ($categories) { ?>
          <nav class="header-nav">
            <ul class="header-nav_list">
              <?php foreach ($categories as $category) { ?>
              <?php if ($category['children']) { ?>
              <li class="header-nav_item animated fadeInUp delay-1s">
                <a href="<?php echo $category['href']; ?>" class="header-nav_link"><?php echo $category['name']; ?></a>
                <div class="header-nav_content">
                  <div class="header-nav_wrap">
                    <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
                    <ul class="header-sub-nav">
                      <?php foreach ($children as $child) { ?>
                      <li class="header-sub-nav_item <?php if ($child['children']) { echo 'header-sub-nav_item__more' ;};?>">
                        <a href="<?php echo $child['href']; ?>" class="header-sub-nav_link" data-sub-number-1><?php echo $child['name']; ?></a>
                        <?php if ($child['children']) { ?>
                          <ul class="header-sub-nav-roster">
                              <?php foreach ($child['children'] as $sub_child) { ?>
                                <li class="header-sub-nav-roster_item">
                                  <a href="<?php echo $sub_child['href']; ?>" class="header-sub-nav-roster_link"><?php echo $sub_child['name']; ?></a>
                                </li>
                              <?php } ?>                            
                          </ul>
                         <?php } ?>
                      </li>
                      <?php } ?>
                      <!-- <li class="header-sub-nav_item">
                        <a href="" class="header-sub-nav_link" data-sub-number-2>Светодиодные модули</a>
                      </li>
                      <li class="header-sub-nav_item header-sub-nav_item__more">
                        <a href="" class="header-sub-nav_link" data-sub-number-3>Светодиодные ленты</a>
                        <ul class="header-sub-nav-roster">
                          <li class="header-sub-nav-roster_item">
                            <a href="" class="header-sub-nav-roster_link">SMD</a>
                          </li>
                          <li class="header-sub-nav-roster_item">
                            <a href="" class="header-sub-nav-roster_link">Пиксели</a>
                          </li>
                          <li class="header-sub-nav-roster_item">
                            <a href="" class="header-sub-nav-roster_link">Торцовые</a>
                          </li>
                        </ul>
                      </li>
                      <li class="header-sub-nav_item">
                        <a href="" class="header-sub-nav_link" data-sub-number-4>Светодиодные линейки</a>
                      </li>
                      <li class="header-sub-nav_item">
                        <a href="" class="header-sub-nav_link" data-sub-number-5>Светодиодные прожекторы</a>
                      </li>
                      <li class="header-sub-nav_item">
                        <a href="" class="header-sub-nav_link" data-sub-number-6>Управление светом/контроллеры</a>
                      </li>
                      <li class="header-sub-nav_item">
                        <a href="" class="header-sub-nav_link" data-sub-number-7>Источники питания</a>
                      </li> -->
                    </ul>
                    <?php } ?>
                  </div>
                </div>
              </li>
            <?php } else { ?>
            <li class="header-nav_item animated fadeInUp delay-1s">
                <a href="<?php echo $category['href']; ?>" class="header-nav_link"><?php echo $category['name']; ?></a>
            </li>
            <?php } ?>
            <?php } ?>

            <!--   <li class="header-nav_item animated fadeInUp delay-1s">
                <a href="" class="header-nav_link">Лампы</a>
              </li>
              <li class="header-nav_item animated fadeInUp delay-1s">
                <a href="" class="header-nav_link">Архитектурное</a>
                <div class="header-nav_content">
                  <div class="header-nav_wrap">
                    <ul class="header-sub-nav">
                      <li class="header-sub-nav_item">
                        <a href="" class="header-sub-nav_link" data-sub-number-1>Клеи и очистители</a>
                      </li>
                      <li class="header-sub-nav_item">
                        <a href="" class="header-sub-nav_link" data-sub-number-2>Светодиодные модули</a>
                      </li>
                      <li class="header-sub-nav_item header-sub-nav_item__more">
                        <a href="" class="header-sub-nav_link" data-sub-number-3>Светодиодные ленты</a>
                        <ul class="header-sub-nav-roster">
                          <li class="header-sub-nav-roster_item">
                            <a href="" class="header-sub-nav-roster_link">SMD</a>
                          </li>
                          <li class="header-sub-nav-roster_item">
                            <a href="" class="header-sub-nav-roster_link">Пиксели</a>
                          </li>
                          <li class="header-sub-nav-roster_item">
                            <a href="" class="header-sub-nav-roster_link">Торцовые</a>
                          </li>
                        </ul>
                      </li>
                      <li class="header-sub-nav_item">
                        <a href="" class="header-sub-nav_link" data-sub-number-4>Светодиодные линейки</a>
                      </li>
                      <li class="header-sub-nav_item">
                        <a href="" class="header-sub-nav_link" data-sub-number-5>Светодиодные прожекторы</a>
                      </li>
                      <li class="header-sub-nav_item">
                        <a href="" class="header-sub-nav_link" data-sub-number-6>Управление светом/контроллеры</a>
                      </li>
                      <li class="header-sub-nav_item">
                        <a href="" class="header-sub-nav_link" data-sub-number-7>Источники питания</a>
                      </li>
                    </ul>
                  </div>
                </div>
              </li>
              <li class="header-nav_item animated fadeInUp delay-1s">
                <a href="" class="header-nav_link">Домашнее</a>
              </li>
              <li class="header-nav_item animated fadeInUp delay-1s">
                <a href="" class="header-nav_link">Промышленное</a>
              </li> -->
            </ul>
          </nav>
          <?php } ?>
          <div class="header-mobile-footer">
            <div class="header-footer_top-block">
              <div class="header-auth_block">
                <?php if ($logged) { ?>
                  <a href="<?php echo $account; ?>" class="header-auth_link">Аккаунт</a>
                  <a href="<?php echo $logout; ?>" class="header-auth_link">Выйти</a>
                <?php } else { ?>
                  <a href="<?php echo $register; ?>" class="header-auth_link">Вход</a>
                  <a href="<?php echo $login; ?>" class="header-auth_link">Регистрация</a>
                <?php } ?>
              </div>
              <div class="header-mobile_contact-block">
                <a href="" class="header-contact_link">Контакты</a>
              </div>
            </div>
            <div class="header-footer_middle-block">
              <div class="header-mobile_city-phone">
                <div class="header-city_block">
                  <p class="header-city_name">г. Воронеж</p>
                </div>
                <div class="header-phone_block">
                  <a href="" class="header-phone_link"><?php echo $telephone; ?></a>
                </div>
              </div>
              <div class="header-mobile_city-phone">
                <div class="header-city_block">
                  <p class="header-city_name">г. Волгоград</p>
                </div>
                <div class="header-phone_block">
                  <a href="" class="header-phone_link"><?php echo $telephone; ?></a>
                  <a href="" class="header-phone_link"><?php echo $telephone; ?></a>
                </div>
              </div>
            </div>
            <div class="header-footer_bottom-block">
              <a href="" class="header-upload_link header-upload_link__mobile" data-project-form-js>загрузить проект</a>
            </div>
          </div>
        </div>
        <div class="header-interaction_block header-interaction__fixed">
          <div class="header-upload_block animated delay-1s">
            <a href="" class="header-upload_link" data-project-form-js="">загрузить проект</a>
          </div>
          <div class="header-interaction_basket-block animated delay-1s">
            <?php echo $cart; ?>
          </div>
        </div>
        <div class="header-search animated fadeInUp delay-1s">
          <?php echo $search; ?>
        </div>
      </div>
    </header>