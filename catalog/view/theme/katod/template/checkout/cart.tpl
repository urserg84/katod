<?php echo $header; ?>
<section class="basket">
  <div class="basket_content">
    <div class="article-page_breadcrumbs">
      <ul itemscope="" itemtype="http://schema.org/BreadcrumbList" class="article-breadcrumbs_list">
        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem" class="article-breadcrumbs_item">
          <a itemprop="item" href="/" class="article-breadcrumbs_link"><span itemprop="name">Главная</span></a>
          <meta itemprop="position" content="1">
        </li>
        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem" class="article-breadcrumbs_item">
          <span itemprop="item" class="article-breadcrumbs_text"><span itemprop="name">Корзина покупок</span></span>
          <meta itemprop="position" content="2">
        </li>
      </ul>
    </div>
    <?php if ($attention) { ?>
      <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $attention; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
      </div>
    <?php } ?>
    <?php if ($success) { ?>
      <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
      </div>
    <?php } ?>
    <?php if ($error_warning) { ?>
      <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
      </div>
    <?php } ?>
    <div class="basket_head">
      <p class="basket_title">Ваша корзина</p>
      <p class="basket_text">Внимание! Доставка оплачивается отдельно.</p>
    </div>
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="basket-wrapper basket-wrapper-form">
      <div class="basket-roster">
        <div class="basket-roster_head">
          <span class="basket-head_name">Наименование товара</span>
          <span class="basket-head_price">Цена</span>
          <span class="basket-head_count">Количество</span>
          <span class="basket-head_total-price">Сумма</span>
        </div>
        <?php foreach ($products as $product) { ?>
          <div class="basket-roster_item">
            <div class="basket-name">
              <div class="basket-name_content">
                <?php if ($product['thumb']) { ?>
                  <div class="basket-name_img">
                    <img src="<?php echo $product['thumb']; ?>" alt="">
                  </div>
                <?php } ?>
                <div class="basket-name_wrap">
                  <span class="basket-name_title"><?php echo $product['name']; ?></span>
                  <?php if (!$product['stock']) { ?>
                  <span class="text-danger">***</span>
                  <?php } ?>
                  <p class="basket-name_property"><?php echo $product['option'][0]['value']; ?></p>   
                  <p class="basket-name_property">Код товара: </p>
                </div>
              </div>
            </div>
            <div class="basket-price">
              <span><?php echo $product['price']; ?></span>
            </div>
            <div class="basket-count">
              <span class="cart-grid_property">
                <a href="" class="cart-product_count__dec" data-dec-js-cart>
                  <svg width="18" height="3" viewBox="0 0 18 3" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M17.5 0V1.5H0V0H17.5Z" transform="translate(0.0161133 0.96582)" fill="#FF9500"/>
                  </svg>
                </a>
                <input type="text" class="cart-product_count" readonly="readonly" value="<?php echo $product['quantity']; ?>" readonly>
                <input type="hidden" class="cart-product_count-hidden" name="quantity[<?php echo $product['cart_id']; ?>]" value="<?php echo $product['quantity']; ?>" >
                <a href="" class="cart-product_count__inc" data-inc-js-cart>
                  <svg width="18" height="19" viewBox="0 0 18 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M9.5 0H8V8H0V9.5H8V17.5H9.5V9.5H17.5V8H9.5V0Z" transform="translate(0.0161133 0.96582)" fill="#FF9500"/>
                  </svg>
                </a>
              </span>
            </div>
            <div class="basket-total-price">
              <span><?php echo $product['total']; ?></span>
              <a href="#" onclick="cart.remove('<?php echo $product['cart_id']; ?>');" title="Удалить" class="basket-item_delete"></a>
            </div>
          </div>
        <?php } ?>
      </div>
      <div class="basket-end">
        <div class="basket-end_desktop">
          <p class="basket-end_total">Итого: <?php echo $totals[0]['text']; ?></p>           
          <a href="<?php echo $continue; ?>" class="basket-end_btn-continue  basket-end_btn-continue__desktop">Продолжить покупки</a>
        </div>
        <div class="basket-end_btn">
          <a href="<?php echo $checkout; ?>" class="basket-end_btn-order" data-basket-order-js>Оформить заказ</a>
        </div>
      </div>
    </form>
  </div>
</section>
<?php echo $content_bottom; ?>
<?php echo $column_right; ?>
<?php echo $footer; ?>