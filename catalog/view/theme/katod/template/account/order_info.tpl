<?php echo $header; ?>
<section class="lk-order-page">
  <div class="lk-order-page_content">
      <div class="article-page_breadcrumbs">
        <ul itemscope="" itemtype="http://schema.org/BreadcrumbList" class="article-breadcrumbs_list">
          <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem" class="article-breadcrumbs_item">
            <a itemprop="item" href="/" class="article-breadcrumbs_link"><span itemprop="name">Главная</span></a>
            <meta itemprop="position" content="<b>Notice</b>: Undefined variable: key in <b>/var/www/katod.com/catalog/view/theme/katod/template/account/order_info.tpl</b> on line <b>8</b>">
          </li>
          <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem" class="article-breadcrumbs_item">
            <a itemprop="item" href="/index.php?route=account/account" class="article-breadcrumbs_link"><span itemprop="name">
                Личный Кабинет</span></a>
            <meta itemprop="position" content="1">
          </li>
          <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem" class="article-breadcrumbs_item">
            <span itemprop="item" class="article-breadcrumbs_text">
              <span itemprop="name">Заказ</span>
            </span>
            <meta itemprop="position" content="3">
          </li>
        </ul>
      </div>
      <?php if ($success) { ?>
      <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
      </div>
      <?php } ?>
      <?php if ($error_warning) { ?>
      <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
      </div>
      <?php } ?>
      <div class="lk-order-page__desktop">
          <p class="lk-order-page_title">Заказ № <?php echo $order_id; ?></p>
          <a href="" class="lk-func_change-btn lk-func_change-btn__download lk-func_change-btn__download__desktop">скачать счет</a>
      </div>
      <div class="lk-order-page_status">
          <div class="lk-status_block">
              <span class="lk-status_name">Дата:</span>
              <span class="lk-status_value"><?php echo $date_added; ?></span>
          </div>
          <div class="lk-status_block">
              <span class="lk-status_name">Статус:</span>
              <span class="lk-status_value lk-status_value__done"><?php echo $payment_address['status']; ?></span>
          </div>
      </div>
      <div class="lk-conditions">
          <p class="lk-conditions_title">Условия:</p>
          <ul class="lk-info_roster">
              <li class="lk-info_item">
                  <span>ФИО:</span>
                  <span><?php echo $payment_address['firstname'] . ' ' . $payment_address['lastname']; ?></span>
              </li>
              <li class="lk-info_item">
                  <span>Телефон:</span>
                  <span><?php echo $payment_address['telephone']; ?></span>
              </li>
              <li class="lk-info_item">
                  <span>Email:</span>
                  <span><?php echo $payment_address['email']; ?></span>
              </li>
              <li class="lk-info_item">
                  <span>Город:</span>
                  <span><?php echo mb_convert_case($payment_address['city'], MB_CASE_TITLE, "UTF-8"); ?></span>
              </li>
              <li class="lk-info_item">
                  <span>Адрес:</span>
                  <span><?php echo $payment_address['address_1']; ?></span>
              </li>
          </ul>
          <div class="lk-func">
              <a href="" class="lk-func_change-btn lk-func_change-btn__download">скачать счет</a>
          </div>
          <div class="lk-order-roster">
              <p class="lk-order_title lk-order_title__page">Состав заказа:</p>
              <div class="lk-order-list">
                  <div class="basket-roster_head lk-order_head">
                      <span class="basket-head_name">Наименование товара</span>
                      <span class="basket-head_price">Цена</span>
                      <span class="basket-head_count">Количество</span>
                      <span class="basket-head_total-price">Сумма</span>
                  </div>
                  <?php foreach ($products as $product) { ?>
                  <div class="lk-order-list_item">
                      <div class="lk-order-name">
                          <div class="lk-order_content">
                              <div class="lk-order-name_img">
                                  <img src= "image/<?php echo $product['image']; ?>" alt="">
                              </div>
                              <div class="lk-order-name_wrap">
                                  <p class="lk-order-name_title"><?php echo $product['name']; ?></p>
                                  <?php foreach ($product['option'] as $option) { ?>
                                  <p class="lk-order-name_property"><?php echo $option['value']; ?></p>
                                  <?php } ?>
                                  <p class="lk-order-name_property">Код товара: </p>
                              </div>
                          </div>
                      </div>
                      <div class="lk-order-price">
                          <span>Цена</span>
                          <span><?php echo $product['price']; ?></span>
                      </div>
                      <div class="lk-order-count">
                          <span>Количество</span>
                          <span class="cart-count_text"><?php echo $product['quantity']; ?></span>
                      </div>
                      <div class="lk-order-total-price">
                          <span>Сумма</span>
                          <span><?php echo $product['total']; ?></span>
                      </div>
                  </div>
                  <?php } ?>
              </div>
              <div class="lk-total-summ">
                  <p class="lk-total-summ_text">Итого: <?php echo $totals['value']; ?>.</p>
              </div>     
          </div>
      </div>
  </div>
</section>
<?php echo $footer; ?>