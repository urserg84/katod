<?php echo $header; ?>
<section class="lk">
  <div class="lk_content">
  <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
  <?php } ?>
    <div class="article-page_breadcrumbs">
      <ul itemscope="" itemtype="http://schema.org/BreadcrumbList" class="article-breadcrumbs_list">
      <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem" class="article-breadcrumbs_item">
        <a itemprop="item" href="/" class="article-breadcrumbs_link"><span itemprop="name">Главная</span></a>
        <meta itemprop="position" content="<?=$key;?>">
      </li>
      <?php foreach ($breadcrumbs as $key => $breadcrumb) { ?>
        <?php if ($key === 0) continue ?>
        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem" class="article-breadcrumbs_item">
          <a itemprop="item" href="<?php echo $breadcrumb['href']; ?>" class="article-breadcrumbs_link"><span itemprop="name">
              <?php echo $breadcrumb['text']; ?></span></a>
          <meta itemprop="position" content="<?=$key;?>">
        </li>
      <?php } ?>
    </ul>
    </div>
    <p class="lk_title">Личный кабинет</p>
    <div class="lk_info">
      <p class="lk-info_title">Информация:</p>
      <ul class="lk-info_roster">
        <li class="lk-info_item">
          <span>ФИО:</span>
          <span><?php echo $firstname.' '.$lastname ?></span>
        </li>
        <li class="lk-info_item">
          <span>Телефон:</span>
          <span><?php echo $telephone; ?></span>
        </li>
        <li class="lk-info_item">
          <span>Email:</span>
          <span><?php echo $email; ?></span>
        </li>
        <li class="lk-info_item">
          <span>Город:</span>
          <span><?php echo $city; ?></span>
        </li>
        <li class="lk-info_item">
          <span>Адрес:</span>
          <span><?php echo $address_1; ?></span>
        </li>
      </ul>
      <div class="lk-func">
        <div>
          <a href="<?php echo $edit; ?>" class="lk-func_change-btn">изменить данные</a>
          <a href="<?php echo $address; ?>" class="lk-func_change-btn">изменить адреса</a>
        </div>
        <a href="<?php echo $password; ?>" class="lk-func_pass">сменить пароль</a>
      </div>
      <div class="lk-order">
        <p class="lk-order_title">Мои заказы:</p>
        <ul class="lk-order_roster">
        <?php if ($orders) { ?>
          <li class="lk-order_head">
            <span>Дата</span>
            <span>Номер заказа</span>
            <span>Сумма</span>
            <span>Статус</span>
          </li>
          <?php foreach ($orders as $order) { ?>           
            <li class="lk-order_body">              
              <p><span>Дата</span><span><?php echo $order['date_added']; ?></span></p>
              <p><span>Номер заказа</span><span class="order_id_span"><?php echo $order['order_id']; ?></span></p>
              <p><span>Сумма</span><span><?php echo $order['total']; ?></span></p>
              <p><span>Статус</span><?php echo $order['status']; ?></p>
            </li>            
          <?php } ?>
        <?php } ?>
        </ul>
      </div>
    </div>
  </div>
</section>
<?php echo $footer; ?> 