<?php echo $header; ?>
<section class="catalog">
  <div class="catalog_content">
    <div class="article-page_breadcrumbs">
      <ul itemscope="" itemtype="http://schema.org/BreadcrumbList" class="article-breadcrumbs_list">
        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem" class="article-breadcrumbs_item">
          <a itemprop="item" href="/" class="article-breadcrumbs_link"><span itemprop="name">Главная</span></a>
          <meta itemprop="position" content="1">
        </li>
        <?php foreach ($breadcrumbs as $key => $breadcrumb) { ?>
        <?php if ($key === 0) continue ?>
        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem" class="article-breadcrumbs_item">
          <a itemprop="item" href="<?php echo $breadcrumb['href']; ?>" class="article-breadcrumbs_link"><span itemprop="name"><?php echo $breadcrumb['text']; ?></span></a>
          <meta itemprop="position" content="<?php echo $key + 2;?>">
        </li>
        <?php } ?>
      </ul>
    </div>
  </div>
</section>
<section class="catalog-base">
  <div class="catalog-base_content">
    <div class="catalog-sort">
        <p class="catalog-sort_text">Сортировать:</p>
        <select name="" id="sort-select-mobile" class="catalog-sort_select catalog-sort_select_search">
          <option value="rating" data-sort="&sort=rating&order=DESC">по популярности</option>
          <option value="p.price" data-sort="&sort=p.price&order=ASC">по цене</option>
          <option value="reminder" data-sort="&sort=reminder&order=DESC">по остатку</option>
        </select>
      </div>
    <div class="catalog-base_right">
      <div class="catalog-base_desktop-sort">
        <form action="" class="catalog-base_desktop-sort_form">
          <p class="catalog-base_desktop-sort_title">Сортировать:</p>
          <ul class="catalog-base_desktop-sort_list">
            <li class="catalog-base_desktop-sort_item catalog-base_desktop-sort_item_search"><a href="#" id="price-sort-link" class="catalog-sort_price-link" data-sort="&sort=p.price&order=ASC">по цене</a></li>
            <li class="catalog-base_desktop-sort_item catalog-base_desktop-sort_item_search"><a href="#" id="popularity-sort-link" data-sort="&sort=rating&order=DESC">по популярности</a></li>
            <li class="catalog-base_desktop-sort_item catalog-base_desktop-sort_item_search"><a href="#" data-sort="&sort=reminder&order=DESC">по остатку</a></li>
          </ul>
        </form>
      </div>
      <div class="catalog-product">
        <?php if ($products) { ?>
          <?php foreach ($products as $product) { ?>
          <div class="carousel-slide carousel-slide__catalog">
            <div class="carousel-slide_head">
            <?php if($product['top_cyrcle_attr']) { ?>
              <div class="sticker-slide">
                <div>
                  <span class="sticker-slide_text">Замена</span>
                  <span class="sticker-slide_introtext">
                    <?php echo $product['top_cyrcle_attr']; ?>
                  </span>
                </div>
              </div>
              <?php } else { ?>
              <div class="sticker-slide-no-image">
              </div>
              <?php } ?>
              <div class="price-slide">
                <p class="price-slide_title"><?php echo $product['price']; ?></p>
                <?php if($product['special']) { ?>
                <p class="price-slide_text"><?php echo $product['special']; ?> оптом</p>
                <?php } ?>
              </div>
            </div>
            <div class="carousel-slide_img-block">
              <img src="<?php echo $product['thumb']; ?>" alt="">
            </div>
            <div class="carousel-slide_info">
              <p class="carousel-info_title"><?php echo $product['name']; ?></p>
              <ul class="carousel-info_roster">
                <?php if($product['attribute_groups']) { ?>
                  <?php foreach($product['attribute_groups'] as $attribute_group) { ?>
                    <li class="carousel-info_item"><?php echo $attribute_group['text']; ?></li>          
                  <?php } ?>
                <?php } ?>
              </ul>
              <a href="<?php echo $product['href']; ?>" class="carousel-slide_btn">подробнее</a>
            </div>
          </div>
          <?php } ?>
      </div>
      <div class="catalog-nav catalog-nav_page__filter">
        <div class="catalog-nav_page">
          <?php echo $pagination; ?>
        </div>
      </div>
    </div>
  </div>
</section>
  <?php } else { ?>
  <p><?php echo $text_empty; ?></p>
  <?php } ?>
  <?php echo $content_bottom; ?></div>
  <?php echo $column_right; ?></div>
</div>
<script type="text/javascript"><!--
$('#button-search').bind('click', function() {
	url = 'index.php?route=product/search';

	var search = $('#content input[name=\'search\']').prop('value');

	if (search) {
		url += '&search=' + encodeURIComponent(search);
	}

	var category_id = $('#content select[name=\'category_id\']').prop('value');

	if (category_id > 0) {
		url += '&category_id=' + encodeURIComponent(category_id);
	}

	var sub_category = $('#content input[name=\'sub_category\']:checked').prop('value');

	if (sub_category) {
		url += '&sub_category=true';
	}

	var filter_description = $('#content input[name=\'description\']:checked').prop('value');

	if (filter_description) {
		url += '&description=true';
	}

	location = url;
});

$('#content input[name=\'search\']').bind('keydown', function(e) {
	if (e.keyCode == 13) {
		$('#button-search').trigger('click');
	}
});

$('select[name=\'category_id\']').on('change', function() {
	if (this.value == '0') {
		$('input[name=\'sub_category\']').prop('disabled', true);
	} else {
		$('input[name=\'sub_category\']').prop('disabled', false);
	}
});

$('select[name=\'category_id\']').trigger('change');
--></script>
<?php echo $footer; ?>