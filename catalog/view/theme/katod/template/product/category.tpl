<?php echo $header; ?>
<section class="catalog">
  <div class="catalog_content">
    <div class="article-page_breadcrumbs">
      <ul itemscope="" itemtype="http://schema.org/BreadcrumbList" class="article-breadcrumbs_list">
        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem" class="article-breadcrumbs_item">
          <a itemprop="item" href="/" class="article-breadcrumbs_link"><span itemprop="name">Главная</span></a>
          <meta itemprop="position" content="1">
        </li>
        <?php foreach ($breadcrumbs as $key => $breadcrumb) { ?>
        <?php if ($key === 0) continue ?>
        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem" class="article-breadcrumbs_item">
          <a itemprop="item" href="<?php echo $breadcrumb['href']; ?>" class="article-breadcrumbs_link"><span itemprop="name"><?php echo $breadcrumb['text']; ?></span></a>
          <meta itemprop="position" content="<?php echo $key + 2;?>">
        </li>
        <?php } ?>
      </ul>
    </div>
    <div class="catalog-info">
      <h1 class="catalog-info_title">
      <img src="<?php if(isset($image)) echo 'image/'.$image; ?>" class="catalog-info_title-image" alt="Изображение категории" />
        <?php echo $heading_title ?>
      </h1>
      <p class="catalog-info_text">
        <?php if ($descriptionSecond) { ?>
          <?php echo $descriptionSecond; ?>
        <?php } ?>
      </p>
    </div>
  </div>
</section>
<section class="catalog-base">
  <div class="catalog-base_content">
    <div class="catalog-base_left">
      <?php echo $column_left; ?>
    </div>
    <div class="catalog-base_right">
      <div class="catalog-base_desktop-sort">
        <form action="" class="catalog-base_desktop-sort_form">
          <p class="catalog-base_desktop-sort_title">Сортировать:</p>
          <ul class="catalog-base_desktop-sort_list">
            <li class="catalog-base_desktop-sort_item"><a href="#" id="price-sort-link" class="catalog-sort_price-link" data-sort="&sort=p.price&order=ASC">по цене</a></li>
            <li class="catalog-base_desktop-sort_item"><a href="#" id="popularity-sort-link" data-sort="&sort=rating&order=DESC">по популярности</a></li>
            <li class="catalog-base_desktop-sort_item"><a href="#" id="reminder-sort-link" data-sort="&sort=reminder&order=DESC">по остатку</a></li>
          </ul>
        </form>
      </div>
      <div class="catalog-product">
        <?php if ($products) { ?>
          <?php foreach ($products as $product) { ?>
          <div class="carousel-slide carousel-slide__catalog">
            <div class="carousel-slide_head">
            <?php if($product['top_cyrcle_attr']) { ?>
              <div class="sticker-slide">
                <div>
                  <span class="sticker-slide_text">Замена</span>
                  <span class="sticker-slide_introtext">
                    <?php echo $product['top_cyrcle_attr']; ?>
                  </span>
                </div>
              </div>
              <?php } else { ?>
              <div class="sticker-slide-no-image">
              </div>
              <?php } ?>
              <div class="price-slide">
                <p class="price-slide_title"><?php echo $product['price']; ?></p>
                <?php if($product['special']) { ?>
                <p class="price-slide_text"><?php echo $product['special']; ?> оптом</p>
                <?php } ?>
              </div>
            </div>
            <a href="<?php echo $product['href']; ?>">
              <div class="carousel-slide_img-block">
                <img src="<?php echo $product['thumb']; ?>" alt="">              
              </div>
            </a>
            <div class="carousel-slide_info">
              <p class="carousel-info_title"><?php echo $product['name']; ?></p>
              <ul class="carousel-info_roster">
                <?php if($product['attribute_groups']) { ?>
                  <?php foreach($product['attribute_groups'] as $attribute_group) { ?>
                    <li class="carousel-info_item"><?php echo $attribute_group['text']; ?></li>          
                  <?php } ?>
                <?php } ?>
              </ul>
              <a href="<?php echo $product['href']; ?>" class="carousel-slide_btn">подробнее</a>
            </div>
          </div>
          <?php } ?>
        <?php } ?>
      </div>
      <div class="catalog-nav">
        <div class="catalog-nav_page">  
            <?php echo $pagination; ?>
        </div>
        <?php if($show_all_button > 1) { ?>
        <div class="catalog-nav_all">
          <a href="" class="catalog-nav-all_link"><span>показать все товары</span></a>
        </div>
        <?php } ?>
      </div>
    </div>
  </div>
</section>
<section class="form-section animated box">
  <div class="start-form_content" data-parent-form-js>
    <p class="start-form_title">У вас есть вопросы?</p>
    <p class="start-form_text">
      Задайте их нашему менеджеру.
      Оставьте свои данные и наш менеджер свяжется
      с Вами в кратчайший срок
    </p>
    <div class="form_subscription__wrapper">
      <form action="" class="form_subscription" data-submit-form-js>
        <div class="form_subscription__success">
          <div></div>
        </div>
        <input type="text" class="form_subscription_input" placeholder="Ваше имя" name="name">
        <input type="text" class="form_subscription_input" placeholder="Ваш номер телефона" name="phone">
        <button class="form_subscription_btn form_subscription_btn__catalog" disabled data-btn-send-js>Отправить</button>
      </form>
      <label class="start-form_agree-block">
        <input class="basket-modal_checkbox" type="checkbox" data-check-js>
        <span class="basket-modal_custom"></span>
        <span class="basket-modal_text">
          Отправляя свои данные я соглашаюсь с <a href="">политикой конфиденциальности</a> сайта
            и даю согласие на обработку персональных данных
        </span>
      </label>
    </div>
  </div>
</section>
<section class="text-section">
  <div class="text-section_content">
    <?php if ($description) { ?>
      <?php echo $description; ?>
    <?php } ?>
  </div>
</section>
<section class="product-more">
  <div class="product-more_content">
    <p class="product-more_title">Другие категории товаров:</p>
    <ul class="product-more_carousel owl-carousel owl-theme">
    <?php foreach ($allCategories as $category) { ?>
    <?php if ($category['children']) { ?>
      <?php 
      foreach ($category['children'] as $child) { ?>
      <li class="product-more_slide">
        <a href="<?php echo $child['href']; ?>" class="product-more_link">
          <div class="product-more_img-block">
            <img class="product-more_img-block-img" src="/image/<?php echo $child['image']; ?>" />
          </div>
          <p class="product-more_name"><?php echo $child['name']; ?></p>
        </a>
      </li>
      <?php } ?>
    <?php } ?>
    <?php } ?>
    </ul>
  </div>
</section>
<?php echo $content_top; ?>
<?php echo $footer; ?>