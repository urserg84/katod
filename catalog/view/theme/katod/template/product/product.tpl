<?php echo $header; ?>
<section class="cart-info">
  <div class="cart-info_content">
    <div class="article-page_breadcrumbs">
      <ul itemscope="" itemtype="http://schema.org/BreadcrumbList" class="article-breadcrumbs_list">
        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem" class="article-breadcrumbs_item">
          <a itemprop="item" href="/" class="article-breadcrumbs_link"><span itemprop="name">Главная</span></a>
          <meta itemprop="position" content="<?=$key;?>">
        </li>
        <?php foreach ($breadcrumbs as $key => $breadcrumb) { ?>
          <?php if ($key === 0) continue ?>
          <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem" class="article-breadcrumbs_item">
            <a itemprop="item" href="<?php echo $breadcrumb['href']; ?>" class="article-breadcrumbs_link"><span itemprop="name">
              <?php echo $breadcrumb['text']; ?></span>
            </a>
            <meta itemprop="position" content="<?=$key;?>">
          </li>
        <?php } ?>
      </ul>
    </div>
    <span class="cart-info_code">Код товара: GU10/7WCOB</span>
    <h1 class="cart-info_title">
      <?php echo $heading_title; ?>
    </h1>
    <div class="cart-info_product">
      <div class="cart-product">
        <div class="cart-slider owl-carousel owl-theme">
        <?php if ($images) { ?>
          <?php foreach ($images as $image) { ?>
          <div class="cart-slider_slide">
            <img src="<?php echo $image['popup']; ?>" alt="">
          </div>
          <?php } ?>
        <?php } ?>
        </div>
        <div class="cart-product_desktop-show">
          <div class="cart-product_big-img">
            <img src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>">
          </div>
          <ul class="cart-more">
            <?php if ($images) { ?>
              <?php foreach ($images as $image) { ?>
              <li class="cart-more_img">
                <a href="" class="cart-more_link">
                  <img src="<?php echo $image['thumb']; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>">
                </a>
              </li>
              <?php } ?>
            <?php } ?>
          </ul>
        </div>
      </div>
      <div class="cart-specification">
        <div class="cart-specification_price">
          <div class="cart-specification_desktop-block">
            <h1 class="cart-specification_title">
              <?php echo $heading_title; ?>
            </h1>
          </div>
          <div class="cart-price">
            <span class="cart-price_text">от
              <?php 
              if(isset($optionMinPrice)) {
                echo $optionMinPrice;
              } else {
                echo $price;
              }?>
            </span>
            <?php if ($special) { ?>
            <span class="cart-price_subtext">
              <?php echo $special; ?> оптом</span>
            <?php } ?>
          </div>
        </div>
        <div class="cart-definition">
          <div class="cart-definition_info">
            <ul class="cart-definition_list">
              <?php foreach ($attribute_groups as $attribute_group) {?>
                <?php if($attribute_group['attribute_group_id'] == 7) { ?>
                  <?php foreach ($attribute_group['attribute'] as $attribute) {?>
                  <li class="carousel-info_item">
                    <?php echo $attribute['text']; ?>
                  </li>
                  <?php } ?>
                <?php } ?>
              <?php }?>
            </ul>
            <p class="cart-definition_text cart-definition_text__desktop">В течение 90 дней бесплатный возврат!</p>
          </div>
          <div class="cart-advantages">
            <ul class="cart-advantages_list">
              <li class="cart-advantages_item"><span><img src="dist/image/advantages.svg" alt=""></span></li>
              <li class="cart-advantages_item"><span><img src="dist/image/advantages.svg" alt=""></span></li>
              <li class="cart-advantages_item"><span><img src="dist/image/advantages.svg" alt=""></span></li>
              <li class="cart-advantages_item"><span><img src="dist/image/advantages.svg" alt=""></span></li>
              <li class="cart-advantages_item"><span><img src="dist/image/advantages.svg" alt=""></span></li>
              <li class="cart-advantages_item"><span><img src="dist/image/advantages.svg" alt=""></span></li>
            </ul>
            <p class="cart-definition_text cart-definition_text__mobile">В течение 90 дней бесплатный возврат!</p>
          </div>
        </div>
        <div class="cart-grid" id="product">
          <?php if ($options) { ?>
          <div class="cart-grid_mobile">
            <ul class="cart-grid_list">
              <li class="cart-grid_item">
                <span class="cart-grid_name">Температура света:</span>
                <span class="cart-grid_property">
                  <div class="cart-grid_select-content">
                    <select name="" id="" class="cart-grid_select">
                      <?php foreach ($options as $option) { ?>
                        <?php if ($option['type'] == 'checkbox') { ?>
                          <?php foreach ($option['product_option_value'] as $key => $option_value) { ?>
                          <option data-id-attr="<?php echo $key ?>" value="<?php echo $option_value['product_option_value_id']; ?>"
                            name="option[<?php echo $option['product_option_id']; ?>][]">
                            <?php echo $option_value['name']; ?>
                          </option>
                          <?php } ?>
                        <?php } ?>
                      <?php } ?>
                    </select>
                  </div>
                </span>
              </li>
              <?php foreach ($options as $option) { ?>
                <?php if ($option['type'] == 'checkbox') { ?>
                  <?php foreach ($option['product_option_value'] as $key => $option_value) { ?>
                  <div class="attr_mobile_div" data-id-attr="<?php echo $key ?>" style="display:none">
                    <li class="cart-grid_item"></li>
                    <li class="cart-grid_item">
                      <span class="cart-grid_name">Цена:</span>
                      <span class="cart-grid_property">
                        <span data-option-price="<?php if ($option_value['priceNumber']) {  echo $option_value['priceNumber']; }?>">
                            <?php if ($option_value['price']) {  echo $option_value['price']; }?>
                        </span>
                      </span>
                    </li>
                    <li class="cart-grid_item">
                      <span class="cart-grid_name">Количество:</span>
                      <span class="cart-grid_property">
                        <a href="" class="cart-product_count__dec cart-product_count__a" data-dec-js>
                          <svg width="18" height="3" viewBox="0 0 18 3" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M17.5 0V1.5H0V0H17.5Z" transform="translate(0.0161133 0.96582)"
                              fill="#FF9500" />
                          </svg>
                        </a>
                        <input type="text" class="cart-product_count" value="0" readonly>
                        <input type="hidden" id="input-quantity" class="quantity_mobile" name="quantity" value="0" />
                        <a href="" class="cart-product_count__inc cart-product_count__a" data-inc-js>
                          <svg width="18" height="19" viewBox="0 0 18 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M9.5 0H8V8H0V9.5H8V17.5H9.5V9.5H17.5V8H9.5V0Z"
                              transform="translate(0.0161133 0.96582)" fill="#FF9500" />
                          </svg>
                        </a>
                      </span>
                      <input type="hidden" class="product_id_mobile" name="product_id" value="<?php echo $product_id; ?>" />
                      <input type="hidden" class="option_mobile" name="option[<?php echo $option['product_option_id']; ?>][]"
                        value="<?php echo $option_value['product_option_value_id']; ?>" />
                    </li>
                    <li class="cart-grid_item">
                      <span class="cart-grid_name">Остаток на складе:</span>
                      <span class="cart-grid_property">На складе
                        <span>
                        <?php echo $option_value['quantity'] ?>
                        </span>
                      </span>
                    </li>
                  </div>
                  <?php } ?>
                <?php } ?>
              <?php } ?>
            </ul>
          </div>
          <?php } ?>
          <div class="cart-grid_desktop">
            <div class="cart-grid_table">
              <?php if ($options) { ?>
              <table class="cart-table">
                <thead>
                  <tr>
                    <th>Температура света</th>
                    <th>Цена</th>
                    <th>Количество</th>
                    <th>Остаток на складе</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($options as $option) { ?>
                    <?php if ($option['type'] == 'checkbox') { ?>
                      <?php foreach ($option['product_option_value'] as $option_value) { ?>
                      <tr>
                        <td>
                          <?php echo $option_value['name']; ?>
                        </td>
                        <td data-option-price="<?php if ($option_value['priceNumber']) {  echo $option_value['priceNumber']; }?>">
                          <?php if ($option_value['price']) {  echo $option_value['price']; }?>
                        </td>
                        <td>
                          <span class="cart-grid_property">
                            <a href="" class="cart-product_count__dec cart-product_count__a" data-dec-js>
                              <svg width="18" height="3" viewBox="0 0 18 3" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M17.5 0V1.5H0V0H17.5Z" transform="translate(0.0161133 0.96582)"
                                  fill="#FF9500"></path>
                              </svg>
                            </a>
                            <input type="text" class="cart-product_count" value="0" readonly>
                            <input type="hidden" id="input-quantity" class="quantity_desktop" name="quantity" value="0" />
                            <a href="" class="cart-product_count__inc cart-product_count__a" data-inc-js>
                              <svg width="18" height="19" viewBox="0 0 18 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M9.5 0H8V8H0V9.5H8V17.5H9.5V9.5H17.5V8H9.5V0Z"
                                  transform="translate(0.0161133 0.96582)" fill="#FF9500"></path>
                              </svg>
                            </a>
                          </span>
                          <input type="hidden" class="product_id_desktop" name="product_id" value="<?php echo $product_id; ?>" />
                          <input type="hidden" class="option_desktop" name="option[<?php echo $option['product_option_id']; ?>][]"
                            value="<?php echo $option_value['product_option_value_id']; ?>" />
                        </td>
                        <td>На складе
                          <?php echo $option_value['quantity'] ?>
                        </td>
                      </tr>
                      <?php } ?>
                    <?php } ?>
                  <?php } ?>
                <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
          <div class="cart-grid_etc">
            <p class="cart-etc_total">Итого: <span class="cart-etc_total__span">0</span> руб.</p>
            <div>
              <a href="" id="button-cart" class="cart-etc_btn">Добавить в корзину</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="cart-characteristic">
  <div class="cart-characteristic_content">
  <?php if (!empty($attribute_groups)) { ?>
    <?php foreach ($attribute_groups as $attribute_group)  { ?>
    <?php if($attribute_group['attribute_group_id'] == 8) { ?>
    <div class="cart-characteristic_roster">
      <a href="" class="cart-characteristic_link" data-show-char-js>Характеристики товара</a>
      <div class="cart-characteristic_wrap">
        <a href="" class="cart-characteristic_link cart-characteristic_link__back" data-hide-char-js>Характеристики
          товара</a>
        <ul class="cart-characteristic_list">
          <?php foreach ($attribute_group['attribute'] as $attribute) { ?>           
            <li class="cart-characteristic_item">
              <span class="cart-characteristic_name"><?php echo $attribute['name']; ?></span>
              <span class="cart-characteristic_property"><?php echo $attribute['text']; ?></span>
            </li>
          <?php } ?>
        </ul>
        <?php if (isset($specification))  { ?>   
          <a href="/system/storage/download/<?php echo $specification['text']; ?>" class="cart-characteristic_btn">скачать спецификацию</a>
        <?php } ?>      
      </div>
    </div>
    <?php } ?>
    <?php } ?>
  <?php } ?>
    <div class="cart-characteristic_info">
      <?php echo $description; ?>
    </div>
  </div>
</section>
<?php echo $content_top; ?>
<script type="text/javascript">
  $('#button-cart').on('click', function (e) {

      e.preventDefault();
      if ($(".cart-grid_mobile").is(":visible") == true) {
          var dataProduct = $('.product_id_mobile, .quantity_mobile, .option_mobile');
      } else {
          var dataProduct = $('.product_id_desktop, .quantity_desktop, .option_desktop');
      }

      //split big array on array in 3 items
      let dataArr = dataProduct;
      let resArr = [];
      let i, j, temparray, chunk = 3;
      for (i = 0, j = dataArr.length; i < j; i += chunk) {
          resArr.push(dataArr.slice(i, i + chunk));
      }

      function sendAjax(productData) {
          $.ajax({
              url: 'index.php?route=checkout/cart/add',
              type: 'post',
              data: productData,
              dataType: 'json',
              beforeSend: function () {
                  $('#button-cart').button('loading');
              },
              complete: function () {
                  $('#button-cart').button('reset');
              },
              success: function (json) {
                  
                  $('.form-group').removeClass('has-error');

                  if (json['error']) {
                      if (json['error']['option']) {
                          for (i in json['error']['option']) {
                              var element = $('#input-option' + i.replace('_', '-'));

                              if (element.parent().hasClass('input-group')) {
                                  element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                              } else {
                                  element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                              }
                          }
                      }

                      if (json['error']['recurring']) {
                          $('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
                      }

                      // Highlight any found errors
                      $('.text-danger').parent().addClass('has-error');
                  }

                  if (json['success']) {
                      
                      $('.alert, .text-danger').remove();

                      $('.article-breadcrumbs_list').after('<div class="alert alert-success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                      $('.header-basket_count').html(json['total']);

                      $('html, body').animate({ scrollTop: 0 }, 'slow');

                      if ($(".cart-grid_mobile").is(":visible") == true) {
                        $('.cart-info_code').after('<div class="alert alert-success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                      } 

                  }
              },
              error: function (xhr, ajaxOptions, thrownError) {
                  alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
              }
          });
      }

      //add products in loop 
      for (let i = 0; i < resArr.length; i++) {
          sendAjax(resArr[i]);
      }

  });
</script>
<?php echo $footer; ?>
