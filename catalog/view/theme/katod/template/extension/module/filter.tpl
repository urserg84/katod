

<div class="catalog-sort">
  <p class="catalog-sort_text">Сортировать:</p>
  <select name="" id="sort-select-mobile" class="catalog-sort_select">
    <option value="rating" data-sort="&sort=rating&order=DESC">по популярности</option>
    <option value="p.price" data-sort="&sort=p.price&order=ASC">по цене</option>
    <option value="reminder" data-sort="&sort=reminder&order=DESC">по остатку</option>
  </select>
</div>
<div class="catalog-filter">
  <a href="" class="catalog-filter_btn" data-show-filter-js>фильтры</a>
  <div class="catalog-filter_form-block">
    <div class="catalog-filter_head">
      <p class="catalog-form_text">Фильтры</p>
      <span class="catalog-filter_exit" data-hide-filter-js></span>
    </div>
    <div class="catalog-form_wrapper">
      <form action="" class="catalog-form">
        <div class="catalog-price">
          <p class="catalog-price_title">Цена, руб.</p>
          <div class="catalog-price_block">
            <span class="catalog-price_wrap catalog-price_wrap__min">
              <input type="text" class="catalog-price_input catalog-price_input_min" data-price-min-js placeholder="100">
            </span>
            <span class="catalog-price_wrap catalog-price_wrap__max">
              <input type="text" class="catalog-price_input catalog-price_input_max" data-price-max-js placeholder="100000">
            </span>
          </div>
        </div>
        <div class="catalog-check_wrap">
        <?php foreach ($filter_groups as $filter_group) { ?>
          <div class="catalog-check">
            <p class="catalog-check_title"><?php echo $filter_group['name']; ?></p>
            <?php foreach ($filter_group['filter'] as $filter) { ?>
            <label class="catalog-check_type">
              <?php if (in_array($filter['filter_id'], $filter_category)) { ?>                     
              <input class="catalog-check_input" type="checkbox" name="filter[]" value="<?php echo $filter['filter_id']; ?>" checked="checked">                                        
              <?php } else { ?>
              <input class="catalog-check_input" type="checkbox" name="filter[]" value="<?php echo $filter['filter_id']; ?>">
              <?php } ?>
              <span class="catalog-check_custom"></span>
              <span class="catalog-check_name"><?php echo $filter['name']; ?></span> 
            </label>
            <?php } ?>
          </div>
        <?php } ?>
        </div>
        <div class="catalog-form_btn-block">
          <a href="#" id="button-filter" class="catalog-form_btn catalog-form_btn__true">фильтровать</a>
          <a href="#" id="clear-filter" class="catalog-form_btn catalog-form_btn__false">сбросить фильтр</a>
        </div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript">
  filterAndClearFilterClick('<?php echo $baseUrl; ?>');
</script>
