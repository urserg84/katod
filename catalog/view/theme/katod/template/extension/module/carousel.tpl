<?php if ($route == 'common/home' || $route == 'product/product' || $route == 'newsblog/article' || $route == 'product/category')  { ?>
  <section class="partners">
    <div class="partners_content">
      <p class="partners_title animated partner-anim-title">Наши партнеры</p>
      <ul class="partners_roster">
        <?php foreach ($banners as $key => $banner) { ?>
        <li class="partners_item animated partner-anim delay-<?=$key;?>s">
          <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" />
        </li>
        <?php } ?>
      </ul>
    </div>
  </section>
  <?php } else { ?>
    <div id="carousel<?php echo $module; ?>" class="owl-carousel">
    <?php foreach ($banners as $banner) { ?>
    <div class="item text-center">
      <?php if ($banner['link']) { ?>
      <a href="<?php echo $banner['link']; ?>"><img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" /></a>
      <?php } else { ?>
      <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" />
      <?php } ?>
    </div>
    <?php } ?>
  </div>
<?php } ?>