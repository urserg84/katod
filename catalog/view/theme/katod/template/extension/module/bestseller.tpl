<?php foreach ($products as $product) { ?>
 <div class="carousel-slide">
  <div class="carousel-slide_head">
  <?php if($product['top_cyrcle_attr']) { ?>
    <div class="sticker-slide">
      <div>
        <span class="sticker-slide_text">Замена</span>
        <span class="sticker-slide_introtext">
          <?php echo $product['top_cyrcle_attr']; ?>
        </span>
      </div>
    </div>
    <?php } else { ?>
    <div class="sticker-slide-no-image">
    </div>
    <?php } ?>
    <div class="price-slide">
      <p class="price-slide_title"><?php echo $product['price']; ?></p>
      <p class="price-slide_text"><?php echo $product['special']; ?> оптом</p>
    </div>
  </div>
  <a href="<?php echo $product['href']; ?>">
    <div class="carousel-slide_img-block">
      <img src="<?php echo $product['thumb']; ?>" alt="">
    </div>
  </a>
  <div class="carousel-slide_info">
    <p class="carousel-info_title"><?php echo $product['name']; ?></p>
    <ul class="carousel-info_roster">
      <?php if($product['attribute_groups']) { ?>
        <?php foreach($product['attribute_groups'] as $attribute_group) { ?>
          <li class="carousel-info_item"><?php echo $attribute_group['text']; ?></li>
        <?php } ?>
      <?php } ?>      
    </ul>
    <a href="<?php echo $product['href']; ?>" class="carousel-slide_btn">подробнее</a>
  </div>
</div>
<?php } ?>