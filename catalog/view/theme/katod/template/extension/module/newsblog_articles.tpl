<?php foreach($categories as $category) { ?>
    <div class="footer-about_block">
        <p class="footer-about_title"><?=$category['name'] ?></p>
        <ul class="footer-about_list">
            <?php foreach($articles as $article) { ?>
                <?php if($article['category'] === $category['category_id']) { ?>
                    <li class="footer-about_item">
                        <a href="<?=$article['href'];?>" class="footer-about_link"><?=$article['name'];?></a>
                    </li>
                <?php } ?>
            <?php } ?>
        </ul>
    </div>
<?php } ?>
