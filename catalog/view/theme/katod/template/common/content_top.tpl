<?php if ($route == 'common/home') { ?>
    <?php echo $modules[0]; ?> 
    <section class="carousel animated box">
      <div class="carousel_content">
        <p class="carousel_title">Новые поступления</p>
        <?php echo $modules[1]; ?>
        </div>
    </section>
    <section class="carousel animated box">
      <div class="carousel_content">
        <p class="carousel_title">Популярные товары</p>
        <?php echo $modules[2]; ?>
        </div>
    </section>
    <section class="start-form animated box">
      <div class="start-form_content" data-parent-form-js>
        <p class="start-form_title">Будьте в курсе последних новостей</p>
        <p class="start-form_title">Подпишитесь на рассылку!</p>
        <p class="start-form_text">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit.
          Aenean euismod bibendum laoreet. Proin gravida dolor sit
          amet lacus accumsan et viverra justo commodo.
        </p>
        <div class="form_subscription__wrapper">
          <form action="" class="form_subscription" data-submit-form-js>
            <div class="form_subscription__success">
              <div></div>
            </div>
            <input type="text" class="form_subscription_input" name="name" placeholder="Ваше имя">
            <input type="text" class="form_subscription_input" name="mail" placeholder="Ваш email">
            <button class="form_subscription_btn" disabled data-btn-send-js>Подписаться</button>
          </form>
          <label class="start-form_agree-block">
            <input class="basket-modal_checkbox" type="checkbox" data-check-js>
            <span class="basket-modal_custom"></span>
            <span class="basket-modal_text">
            Отправляя свои данные я соглашаюсь с <a href="">политикой конфиденциальности</a> сайта
              и даю согласие на обработку персональных данных
          </span>
          </label>
        </div>
      </div>
    </section>
    <?php echo $modules[3]; ?>
<?php } else if ($route == 'product/product') { ?>
    <section class="carousel carousel__cart animated box">
      <div class="carousel_content">
        <p class="carousel_title carousel_title__cart">Популярные товары из этой категории:</p>
        <div class="carousel-slider owl-carousel owl-theme">
          <?php echo $modules[0]; ?>
        </div>
      </div>
    </section>
    <section class="start-form animated box">
      <div class="start-form_content" data-parent-form-js>
        <p class="start-form_title">Будьте в курсе последних новостей</p>
        <p class="start-form_title">Подпишитесь на рассылку!</p>
        <p class="start-form_text">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit.
          Aenean euismod bibendum laoreet. Proin gravida dolor sit
          amet lacus accumsan et viverra justo commodo.
        </p>
        <div class="form_subscription__wrapper">
          <form action="" class="form_subscription" data-submit-form-js>
            <div class="form_subscription__success">
              <div></div>
            </div>
            <input type="text" class="form_subscription_input" name="name" placeholder="Ваше имя">
            <input type="text" class="form_subscription_input" name="mail" placeholder="Ваш email">
            <button class="form_subscription_btn" disabled data-btn-send-js>Подписаться</button>
          </form>
          <label class="start-form_agree-block">
            <input class="basket-modal_checkbox" type="checkbox" data-check-js>
            <span class="basket-modal_custom"></span>
            <span class="basket-modal_text">
            Отправляя свои данные я соглашаюсь с <a href="">политикой конфиденциальности</a> сайта
              и даю согласие на обработку персональных данных
          </span>
          </label>
        </div>
      </div>
    </section>
    <?php echo $modules[1]; ?>
<?php } else if ($route == 'product/category') { ?>
  <section class="start-form animated box">
    <div class="start-form_content" data-parent-form-js>
      <p class="start-form_title">Будьте в курсе последних новостей</p>
      <p class="start-form_title">Подпишитесь на рассылку!</p>
      <p class="start-form_text">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        Aenean euismod bibendum laoreet. Proin gravida dolor sit
        amet lacus accumsan et viverra justo commodo.
      </p>
      <div class="form_subscription__wrapper">
        <form action="" class="form_subscription" data-submit-form-js>
          <div class="form_subscription__success">
            <div></div>
          </div>
          <input type="text" class="form_subscription_input" name="name" placeholder="Ваше имя">
          <input type="text" class="form_subscription_input" name="mail" placeholder="Ваш email">
          <button class="form_subscription_btn" disabled data-btn-send-js>Подписаться</button>
        </form>
        <label class="start-form_agree-block">
          <input class="basket-modal_checkbox" type="checkbox" data-check-js>
          <span class="basket-modal_custom"></span>
          <span class="basket-modal_text">
          Отправляя свои данные я соглашаюсь с <a href="">политикой конфиденциальности</a> сайта
            и даю согласие на обработку персональных данных
        </span>
        </label>
      </div>
    </div>
  </section>
  <?php echo $modules[0]; ?>
<?php } else { ?>
  <?php foreach ($modules as $module) { ?>
  <?php echo $module; ?>
  <?php } ?>
<?php } ?>