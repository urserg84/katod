<?php echo $header; ?>
<section class="basket">
    <div class="basket_content">
    </ul>
    <div class="row"><?php echo $column_left; ?>
      <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
        <h1 class="start-form_title"><?php echo $heading_title; ?></h1>
        <?php echo $text_message;  ?>
        <?php echo $content_bottom; ?>
      </div>
      <?php echo $column_right; ?>
    </div>
  </div>
</section>
<?php echo $footer; ?>