    <footer class="footer">
      <div class="footer_content">
        <div class="footer-wrap">
          <div class="footer-info">
            <div class="footer-info_img-block">
              <img src="/image/assets/footer-logo.png" alt="">
            </div>
            <div class="footer-info_address">
              <div class="footer-address_block">
                <p class="footer-address_city">г. Волгоград</p>
                <p class="footer-address_street">ул. Череповецкая 11/5</p>
                <p class="footer-address_phone">+7 (8442) 43-80-50, 43-80-51</p>
              </div>
              <div class="footer-address_block">
                <p class="footer-address_city">г. Воронеж</p>
                <p class="footer-address_street">ул.Машиностроителей 2</p>
                <p class="footer-address_phone">+7 (903) 655 61 31</p>
              </div>
            </div>
            <div class="footer-soc">
              <ul class="footer-soc_list">
                <li class="footer-soc_item">
                  <a href="" class="footer-soc_link" target="_blank">
                    <svg width="12" height="8" viewBox="0 0 12 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path d="M11.709 2.91016C11.666 3.125 11.4941 3.49023 11.1934 3.98438C10.9785 4.34961 10.7207 4.75781 10.4199 5.23047C10.1621 5.61719 10.0117 5.81055 10.0117 5.81055C9.92578 5.93945 9.88281 6.04688 9.88281 6.11133C9.88281 6.19727 9.92578 6.2832 10.0117 6.36914L10.2051 6.58398C11.2363 7.6582 11.8164 8.38867 11.9453 8.77539C11.9883 8.94727 11.9668 9.09766 11.8809 9.18359C11.7949 9.26953 11.6875 9.3125 11.5156 9.3125H10.248C10.0762 9.3125 9.94727 9.26953 9.81836 9.16211C9.73242 9.11914 9.56055 8.96875 9.3457 8.71094C9.13086 8.45312 8.9375 8.23828 8.76562 8.06641C8.18555 7.5293 7.75586 7.25 7.49805 7.25C7.36914 7.25 7.2832 7.27148 7.24023 7.31445C7.19727 7.35742 7.17578 7.46484 7.17578 7.59375C7.1543 7.70117 7.1543 7.91602 7.1543 8.25977V8.81836C7.1543 8.99023 7.11133 9.11914 7.02539 9.18359C6.89648 9.26953 6.63867 9.3125 6.25195 9.3125C5.56445 9.3125 4.89844 9.11914 4.21094 8.71094C3.52344 8.32422 2.92188 7.74414 2.40625 6.99219C1.91211 6.34766 1.50391 5.68164 1.18164 4.99414C0.923828 4.47852 0.708984 4.00586 0.558594 3.55469C0.451172 3.21094 0.408203 2.95312 0.408203 2.80273C0.408203 2.56641 0.537109 2.4375 0.837891 2.4375H2.10547C2.25586 2.4375 2.36328 2.48047 2.44922 2.54492C2.53516 2.63086 2.59961 2.75977 2.66406 2.93164C2.85742 3.51172 3.09375 4.07031 3.39453 4.60742C3.65234 5.12305 3.91016 5.53125 4.16797 5.83203C4.42578 6.1543 4.61914 6.30469 4.76953 6.30469C4.85547 6.30469 4.89844 6.2832 4.94141 6.21875C4.98438 6.1543 5.00586 6.02539 5.00586 5.83203V3.96289C4.98438 3.74805 4.94141 3.5332 4.85547 3.36133C4.8125 3.27539 4.72656 3.16797 4.64062 3.03906C4.5332 2.91016 4.49023 2.82422 4.49023 2.73828C4.49023 2.65234 4.51172 2.58789 4.57617 2.52344C4.64062 2.48047 4.72656 2.4375 4.8125 2.4375H6.81055C6.93945 2.4375 7.02539 2.48047 7.06836 2.54492C7.11133 2.63086 7.1543 2.73828 7.1543 2.91016V5.40234C7.1543 5.53125 7.17578 5.63867 7.21875 5.68164C7.26172 5.74609 7.30469 5.76758 7.36914 5.76758C7.43359 5.76758 7.51953 5.74609 7.60547 5.70312C7.69141 5.66016 7.79883 5.55273 7.94922 5.38086C8.20703 5.08008 8.48633 4.71484 8.76562 4.26367C8.9375 3.96289 9.13086 3.61914 9.30273 3.23242L9.51758 2.80273C9.60352 2.56641 9.79688 2.4375 10.0547 2.4375H11.3223C11.666 2.4375 11.7949 2.60938 11.709 2.91016Z" transform="translate(-0.214355 -1.8623)" fill="#2A2D33"/>
                    </svg>
                  </a>
                </li>
                <li class="footer-soc_item">
                  <a href="https://www.instagram.com/tdkatod/" class="footer-soc_link" target="_blank">
                    <svg width="11" height="10" viewBox="0 0 11 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path d="M4.8125 3.4043C5.24219 3.4043 5.65039 3.5332 6.03711 3.74805C6.42383 3.96289 6.72461 4.26367 6.93945 4.65039C7.1543 5.03711 7.2832 5.44531 7.2832 5.875C7.2832 6.32617 7.1543 6.73438 6.93945 7.12109C6.72461 7.50781 6.42383 7.80859 6.03711 8.02344C5.65039 8.23828 5.24219 8.3457 4.8125 8.3457C4.36133 8.3457 3.95312 8.23828 3.56641 8.02344C3.17969 7.80859 2.87891 7.50781 2.66406 7.12109C2.44922 6.73438 2.3418 6.32617 2.3418 5.875C2.3418 5.44531 2.44922 5.03711 2.66406 4.65039C2.87891 4.26367 3.17969 3.96289 3.56641 3.74805C3.95312 3.5332 4.36133 3.4043 4.8125 3.4043ZM4.8125 7.48633C5.24219 7.48633 5.62891 7.33594 5.95117 7.01367C6.25195 6.71289 6.42383 6.32617 6.42383 5.875C6.42383 5.44531 6.25195 5.05859 5.95117 4.73633C5.62891 4.43555 5.24219 4.26367 4.8125 4.26367C4.36133 4.26367 3.97461 4.43555 3.67383 4.73633C3.35156 5.05859 3.20117 5.44531 3.20117 5.875C3.20117 6.32617 3.35156 6.71289 3.67383 7.01367C3.97461 7.33594 4.36133 7.48633 4.8125 7.48633ZM7.9707 3.29688C7.9707 3.14648 7.90625 3.01758 7.79883 2.88867C7.66992 2.78125 7.54102 2.7168 7.39062 2.7168C7.21875 2.7168 7.08984 2.78125 6.98242 2.88867C6.85352 3.01758 6.81055 3.14648 6.81055 3.29688C6.81055 3.46875 6.85352 3.59766 6.98242 3.70508C7.08984 3.83398 7.21875 3.87695 7.39062 3.87695C7.54102 3.87695 7.66992 3.83398 7.77734 3.70508C7.88477 3.59766 7.94922 3.46875 7.9707 3.29688ZM9.60352 3.87695C9.60352 4.28516 9.625 4.95117 9.625 5.875C9.625 6.82031 9.60352 7.48633 9.58203 7.89453C9.56055 8.30273 9.49609 8.64648 9.41016 8.94727C9.28125 9.3125 9.06641 9.63477 8.80859 9.89258C8.55078 10.1504 8.22852 10.3438 7.88477 10.4727C7.58398 10.5801 7.21875 10.6445 6.81055 10.666C6.40234 10.6875 5.73633 10.6875 4.8125 10.6875C3.86719 10.6875 3.20117 10.6875 2.79297 10.666C2.38477 10.6445 2.04102 10.5801 1.74023 10.4512C1.375 10.3438 1.05273 10.1504 0.794922 9.89258C0.537109 9.63477 0.34375 9.3125 0.214844 8.94727C0.107422 8.64648 0.0429688 8.30273 0.0214844 7.89453C0 7.48633 0 6.82031 0 5.875C0 4.95117 0 4.28516 0.0214844 3.87695C0.0429688 3.46875 0.107422 3.10352 0.214844 2.80273C0.34375 2.45898 0.537109 2.13672 0.794922 1.87891C1.05273 1.62109 1.375 1.40625 1.74023 1.27734C2.04102 1.19141 2.38477 1.12695 2.79297 1.10547C3.20117 1.08398 3.86719 1.0625 4.8125 1.0625C5.73633 1.0625 6.40234 1.08398 6.81055 1.10547C7.21875 1.12695 7.58398 1.19141 7.88477 1.27734C8.22852 1.40625 8.55078 1.62109 8.80859 1.87891C9.06641 2.13672 9.28125 2.45898 9.41016 2.80273C9.49609 3.10352 9.56055 3.46875 9.60352 3.87695ZM8.57227 8.71094C8.6582 8.47461 8.70117 8.08789 8.74414 7.55078C8.74414 7.25 8.76562 6.79883 8.76562 6.21875V5.53125C8.76562 4.95117 8.74414 4.5 8.74414 4.19922C8.70117 3.66211 8.6582 3.27539 8.57227 3.03906C8.40039 2.60938 8.07812 2.28711 7.64844 2.11523C7.41211 2.0293 7.02539 1.98633 6.48828 1.94336C6.16602 1.94336 5.71484 1.92188 5.15625 1.92188H4.46875C3.88867 1.92188 3.4375 1.94336 3.13672 1.94336C2.59961 1.98633 2.21289 2.0293 1.97656 2.11523C1.52539 2.28711 1.22461 2.60938 1.05273 3.03906C0.966797 3.27539 0.902344 3.66211 0.880859 4.19922C0.859375 4.52148 0.859375 4.97266 0.859375 5.53125V6.21875C0.859375 6.79883 0.859375 7.25 0.880859 7.55078C0.902344 8.08789 0.966797 8.47461 1.05273 8.71094C1.22461 9.16211 1.54688 9.46289 1.97656 9.63477C2.21289 9.7207 2.59961 9.78516 3.13672 9.80664C3.4375 9.82812 3.88867 9.82812 4.46875 9.82812H5.15625C5.73633 9.82812 6.1875 9.82812 6.48828 9.80664C7.02539 9.78516 7.41211 9.7207 7.64844 9.63477C8.07812 9.46289 8.40039 9.14062 8.57227 8.71094Z" transform="translate(0.951416 -0.862305)" fill="#2A2D33"/>
                    </svg>
                  </a>
                </li>
                <li class="footer-soc_item">
                  <a href="" class="footer-soc_link" target="_blank">
                    <svg width="13" height="10" viewBox="0 0 13 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path d="M11.8164 3.03906C11.9023 3.42578 11.9883 4.02734 12.0312 4.80078L12.0527 5.875L12.0312 6.94922C11.9883 7.76562 11.9023 8.3457 11.8164 8.73242C11.7305 8.99023 11.6016 9.20508 11.4297 9.37695C11.2363 9.57031 11.0215 9.69922 10.7637 9.76367C10.377 9.87109 9.51758 9.93555 8.14258 9.97852L6.1875 10L4.23242 9.97852C2.85742 9.93555 1.97656 9.87109 1.61133 9.76367C1.35352 9.69922 1.11719 9.57031 0.945312 9.37695C0.751953 9.20508 0.623047 8.99023 0.558594 8.73242C0.451172 8.3457 0.386719 7.76562 0.34375 6.94922L0.322266 5.875C0.322266 5.57422 0.322266 5.20898 0.34375 4.80078C0.386719 4.02734 0.451172 3.42578 0.558594 3.03906C0.623047 2.78125 0.751953 2.56641 0.945312 2.37305C1.11719 2.20117 1.35352 2.07227 1.61133 1.98633C1.97656 1.90039 2.85742 1.81445 4.23242 1.77148L6.1875 1.75L8.14258 1.77148C9.51758 1.81445 10.377 1.90039 10.7637 1.98633C11.0215 2.07227 11.2363 2.20117 11.4297 2.37305C11.6016 2.56641 11.7305 2.78125 11.8164 3.03906ZM4.98438 7.63672L8.05664 5.875L4.98438 4.13477V7.63672Z" transform="translate(0.117432 -0.862305)" fill="#2A2D33"/>
                    </svg>
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <div class="footer-about">
            <?php echo $article_module; ?>
          </div>
        </div>
        <div class="footer-copyright">
          <p>Разработано в <a href="http://monodigital.ru/" target="_blank">MONO</a> и <a href="https://todoagency.ru/" target="_blank">TODO</a></p>
        </div>
      </div>
    </footer>
  </div>
  <div class="overlay"></div>
  <?php if($route === 'checkout/cart') { ?>
    <div class="basket-modal" data-parent-form-js>
      <div class="basket-modal_content">
        <p class="basket-modal_title">Оформление заказа</p>
        <span class="basket-modal_exit"></span>
        <form action="" class="basket-modal_form">
          <label class="basket-modal_label">
            <input type="text" placeholder="ФИО" name="firstname" class="basket-modal_input"
            <?php if(isset($customer_data)) {?>
            value = "<?php echo $customer_data['firstname'] . ' ' .$customer_data['lastname']; ?>"
            <?php } ?>
             >
          </label>
          <label class="basket-modal_label">
            <input type="text" placeholder="Номер телефона" name="telephone" class="basket-modal_input"
            <?php if(isset($customer_data)) {?>
            value = "<?php echo $customer_data['telephone']; ?>"
            <?php } ?>
            >
          </label>
          <label class="basket-modal_label">
            <input type="text" placeholder="Email" name="email" class="basket-modal_input"
            <?php if(isset($customer_data)) {?>
            value = "<?php echo $customer_data['email']; ?>"
            <?php } ?>
            >
          </label>
          <label class="basket-modal_label">
            <input type="text" placeholder="Город" name="city" class="basket-modal_input"
            <?php if(isset($customer_data)) {?>
            value = "<?php echo $customer_data['city']; ?>"
            <?php } ?>
            >
          </label>
          <label class="basket-modal_label">
            <input type="text" placeholder="Адрес" name="address_1" class="basket-modal_input"
            <?php if(isset($customer_data)) {?>
            value = "<?php echo $customer_data['address_1']; ?>"
            <?php } ?>
            >
          </label>
          <label class="basket-modal_label">
            <input class="basket-modal_checkbox" type="checkbox" data-check-js>
            <span class="basket-modal_custom"></span>
            <span class="basket-modal_text">
              Отправляя свои данные я соглашаюсь
              с <a href="">политикой конфиденциальности</a> сайта
              и даю согласие на обработку персональных данных
            </span>
          </label>
          <button class="basket-modal_btn confirm_order_btn" disabled data-btn-send-js>Завершить оформление</button>
        </form>
      </div>
    </div>
  <?php } ?>
  <div class="modal-project">
    <span class="modal-project_exit"></span>
    <div class="modal-project_content">
      <div class="modal-project_wrap" data-parent-form-js>
        <div class="modal-project__success">
          <div>
            <p>Ваше сообщение отправлено</p>
          </div>
        </div>
        <p class="modal-project_title">Отправьте нам свой проект и мы обязательно Вам поможем!</p>
        <form action="" class="modal-project_form" enctype="multipart/form-data" data-send-project-js>
          <div class="modal-project_file-upload">
            <label>
              <input type="file" name="file" id="downloadProject">
              <img src="dist/image/upload-file.svg" alt="">
              <span>Нажмите чтобы прикрепить файл проекта</span>
            </label>
          </div>
          <div class="modal-project_wrapper">
            <div class="modal-project_left">
              <label class="modal-project_label">
                <input type="text" class="modal-project_input" name="name" placeholder="Ваше имя">
              </label>
              <label class="modal-project_label">
                <input type="text" class="modal-project_input" name="phone" placeholder="Номер телефона">
              </label>
              <label class="modal-project_label">
                <input type="text" class="modal-project_input" name="mail" placeholder="Email">
              </label>
            </div>
            <div class="modal-project_right">
              <label class="modal-project_label">
                <textarea name="message" class="modal-project_textarea" placeholder="Комментарий"></textarea>
              </label>
            </div>
          </div>
          <button class="modal-project_btn" disabled data-modal-send-js>Отправить</button>
        </form>
        <label class="start-form_agree-block">
          <input class="basket-modal_checkbox" type="checkbox" data-check-js>
          <span class="basket-modal_custom"></span>
          <span class="basket-modal_text">
            Отправляя свои данные я соглашаюсь с <a href="">политикой конфиденциальности</a> сайта
              и даю согласие на обработку персональных данных
          </span>
        </label>
      </div>
    </div>
  </div>

  <script src="/catalog/view/theme/katod/js/manifest.js?l"></script>
  <script src="/catalog/view/theme/katod/js/vendor.js?l"></script>
  <script src="/catalog/view/theme/katod/js/app.js"></script>
  <script src="/catalog/view/theme/katod/js/script.js"></script>
  <script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
  <script src="catalog/view/javascript/common.js" type="text/javascript"></script>
  <script src="/catalog/view/theme/katod/js/jquery.waypoints.js"></script>
  <script>
    $('.box').waypoint( function(dir) {
        if (dir === 'down')
            $(this).addClass('fadeIn');
    }, {
        offset: '50%'
    });

    $('.partner-anim-title').waypoint( function(dir) {
        if (dir === 'down')
            $(this).addClass('fadeIn');
            $('.partner-anim').addClass('fadeIn')
    },{
        offset: '70%'
    })
  </script>
</body>
</html>