<div class="header-basket_block">
  <a href="<?php echo $cart; ?>" class="header-basket_link">
    <div class="header-basket_img-block">
      <img src="/image/assets/basket-logo.svg" alt="">
      <span class="header-basket_count"><?php echo $count_items; ?></span>
    </div>
    <span class="header-basket_text">ваша корзина</span>
  </a>
</div>