<?php echo $header; ?>
<?php if($information_id === 4) { ?>
  <div class="container">
    <ul class="breadcrumb">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
      <?php } ?>
    </ul>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>   
        <?php echo $description; ?>
        <script async="" defer="" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyApbceK2yZ-XiXkXboop7bKvrxjbpVVXXM&amp;callback=initMap"></script>
        <script>
            function initMap() {
              var uluru = {lat: 48.696589, lng: 44.496842};
              var map = new google.maps.Map(document.getElementById('contact_map'), {
                  zoom: 16,
                  center: uluru,
                  controls: [],
                  disableDefaultUI: true,
                  styles: [

                  ]
              });

              var marker = new google.maps.Marker({
                  position: uluru,
                  map: map
              });
            }
            $('.box').waypoint( function(dir) {
                if (dir === 'down')
                  $(this).addClass('fadeIn');
            }, {
                offset: '50%'
            });

            $('.partner-anim-title').waypoint( function(dir) {
                if (dir === 'down')
                  $(this).addClass('fadeIn');
                $('.partner-anim').addClass('fadeIn')
            },{
                offset: '70%'
            })
        </script>
        <section class="start-form start-form__contact animated box">
          <div class="start-form_content" data-parent-form-js>
            <p class="start-form_title">Будьте в курсе последних новостей</p>
            <p class="start-form_title">Подпишитесь на рассылку!</p>
            <p class="start-form_text">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit.
              Aenean euismod bibendum laoreet. Proin gravida dolor sit
              amet lacus accumsan et viverra justo commodo.
            </p>
            <div class="form_subscription__wrapper">
              <form action="" class="form_subscription" data-submit-form-js>
                <div class="form_subscription__success">
                  <div></div>
                </div>
                <input type="text" class="form_subscription_input" name="name" placeholder="Ваше имя">
                <input type="text" class="form_subscription_input" name="mail" placeholder="Ваш email">
                <button class="form_subscription_btn" disabled data-btn-send-js>Подписаться</button>
              </form>
              <label class="start-form_agree-block">
                <input class="basket-modal_checkbox" type="checkbox" data-check-js>
                <span class="basket-modal_custom"></span>
                <span class="basket-modal_text">
                  Отправляя свои данные я соглашаюсь с <a href="">политикой конфиденциальности</a> сайта
                    и даю согласие на обработку персональных данных
                </span>
              </label>
            </div>
          </div>
        </section>
      <?php } else {?>
        <section class="article-page">
          <div class="article-page_content">
            <div class="article-page_breadcrumbs">
              <ul itemscope="" itemtype="http://schema.org/BreadcrumbList" class="article-breadcrumbs_list">
                <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem" class="article-breadcrumbs_item">
                    <a itemprop="item" href="/" class="article-breadcrumbs_link"><span itemprop="name">Главная</span></a>
                    <meta itemprop="position" content="1">
                </li>
                <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem" class="article-breadcrumbs_item">
                    <span itemprop="item" class="article-breadcrumbs_text"><span itemprop="name"><?php echo $heading_title; ?></span></span>
                    <meta itemprop="position" content="2">
                </li>
              </ul>
            </div>
            <h1 class="article-page_title"><?php echo $heading_title; ?></h1>
            <div class="article-page_wrap">
              <div class="article-page_left">
                <div class="article-page_info">
                  <div class="article-info_block">
                    <img src="dist/image/article.jpg" alt="">
                    <div class="article-info_text">
                      <p>
                        Блок оптимизированного текста.
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod
                        bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et
                        viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque
                        penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam
                        fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget.
                      </p>
                      <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum
                        laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo.
                        Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis
                        parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus
                        pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget.
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum
                        laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo.
                          Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis
                          parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra
                          vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget.Lorem ipsum
                          dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.
                          Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin
                          sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient
                          montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate,
                            felis tellus mollis orci, sed rhoncus sapien nunc eget.Lorem ipsum dolor sit
                            amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin
                            gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales
                            pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes,
                            nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis
                            tellus mollis orci, sed rhoncus sapien nunc eget.
                      </p>
                      <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum
                        laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo.
                        Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis
                        parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus
                        pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget.
                      </p>
                    </div>
                  </div>
                  <div class="article-info_block">
                    <img src="dist/image/article.jpg" alt="">
                    <div class="article-info_text">
                      <p>
                        Блок оптимизированного текста.
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod
                        bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et
                        viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque
                        penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam
                        fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget.
                      </p>
                      <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum
                        laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo.
                        Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis
                        parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus
                        pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget.
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum
                        laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo.
                          Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis
                          parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra
                          vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget.Lorem ipsum
                          dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.
                          Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin
                          sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient
                          montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate,
                            felis tellus mollis orci, sed rhoncus sapien nunc eget.Lorem ipsum dolor sit
                            amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin
                            gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales
                            pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes,
                            nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis
                            tellus mollis orci, sed rhoncus sapien nunc eget.
                      </p>
                      <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum
                        laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo.
                        Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis
                        parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus
                        pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget.
                      </p>
                    </div>
                  </div>
                </div>
                <div class="article-page_proffer">
                  <p class="article-proffer_title">Вам будет интересно:</p>
                  <div class="article-proffer_slide">
                    <div class="article-slide owl-carousel owl-theme">
                      
                      
                      
                    <div class="article-slide_item">
                        <img src="dist/image/article.jpg" alt="">
                        <p class="article-slide_name">Название статьи</p>
                      </div><div class="article-slide_item">
                        <img src="dist/image/article.jpg" alt="">
                        <p class="article-slide_name">Название статьи</p>
                      </div><div class="article-slide_item">
                        <img src="dist/image/article.jpg" alt="">
                        <p class="article-slide_name">Название статьи</p>
                      </div></div>
                  </div>
                </div>
              </div>
              <div class="article-page_right">
                <div class="article-page_recom-block">
                  <p class="article-page_recom_title">Рекомендуем по этой теме:</p>
                  <ul class="article-recommendation_list">
                    <li class="article-recommendation_item">
                      <a href="" class="article-recommendation_link">
                        <img src="dist/image/article.jpg" alt="">
                        <p class="article-recommendation_title">Название статьи</p>
                      </a>
                    </li>
                    <li class="article-recommendation_item">
                      <a href="" class="article-recommendation_link">
                        <img src="dist/image/article.jpg" alt="">
                        <p class="article-recommendation_title">Название статьи</p>
                      </a>
                    </li>
                  </ul>
                </div>
                <div class="article-advice">
                  <p class="article-advice_title">Читайте далее:</p>
                  <ul class="article-advice_list">
                    <li class="article-advice_item">
                      <a href="" class="article-advice_link">Как подобрать светильник</a>
                    </li>
                    <li class="article-advice_item">
                      <a href="" class="article-advice_link">Как подобрать светильник</a>
                    </li>
                    <li class="article-advice_item">
                      <a href="" class="article-advice_link">Как подобрать светильник</a>
                    </li>
                    <li class="article-advice_item">
                      <a href="" class="article-advice_link">Как подобрать светильник</a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </section>
        <?php echo $description; ?>
        <?php echo $content_bottom; ?></div>
        <?php echo $column_right; ?></div>
    <?php } ?>
<?php echo $footer; ?>