<?php echo $header; ?>
<section class="article-page">
  <div class="article-page_content">
    <div class="article-page_breadcrumbs">
      <ul itemscope="" itemtype="http://schema.org/BreadcrumbList" class="article-breadcrumbs_list">
        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem" class="article-breadcrumbs_item">
          <a itemprop="item" href="/" class="article-breadcrumbs_link"><span itemprop="name">Главная</span></a>
          <meta itemprop="position" content="1">
        </li>
        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem" class="article-breadcrumbs_item">
          <span itemprop="item" class="article-breadcrumbs_text"><span itemprop="name"><?php echo $heading_title; ?></span></span>
          <meta itemprop="position" content="2">
        </li>
      </ul>
    </div>
    <h1 class="article-page_title"><?php echo $heading_title; ?></h1>
    <div class="article-page_wrap">
      <div class="article-page_left">
        <div class="article-page_info">
          <?php echo $description; ?>
        </div>
        <?php if ($articles) { ?>
        <div class="article-page_proffer">
          <p class="article-proffer_title">Вам будет интересно:</p>
          <div class="article-proffer_slide">
            <div class="article-slide owl-carousel owl-theme">
              <?php foreach ($articles as $article) { ?>
              <div class="article-slide_item">
                <img src="image/<?php echo $article['image'][0]['image']; ?>" alt="">
                <a href="<?php echo $article['href']; ?>"><p class="article-slide_name"><?php echo $article['name']; ?></p></a>
              </div>
              <?php } ?>          
            </div>
          </div>
        </div>
        <?php } ?>  
      </div>
      <div class="article-page_right">
        <div class="article-page_recom-block">
          <p class="article-page_recom_title">Рекомендуем по этой теме:</p>
          <ul class="article-recommendation_list">
          <?php if ($rand_articles) { ?>
              <?php foreach ($rand_articles as $rand_article) { ?>
              <li class="article-recommendation_item">
                <a href="<?php echo $rand_article['href']; ?>" class="article-recommendation_link">
                  <img src="image/<?php echo $rand_article['image']; ?>" alt="">
                  <p class="article-recommendation_title"><?php echo $rand_article['name']; ?></p>
                </a>
              </li>
              <?php } ?>
          <?php } ?>      
          </ul>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="start-form start-form__contact animated box">
  <div class="start-form_content" data-parent-form-js>
    <p class="start-form_title">Будьте в курсе последних новостей</p>
    <p class="start-form_title">Подпишитесь на рассылку!</p>
    <p class="start-form_text">
      Lorem ipsum dolor sit amet, consectetur adipiscing elit.
      Aenean euismod bibendum laoreet. Proin gravida dolor sit
      amet lacus accumsan et viverra justo commodo.
    </p>
    <div class="form_subscription__wrapper">
      <form action="" class="form_subscription" data-submit-form-js>
        <div class="form_subscription__success">
          <div></div>
        </div>
        <input type="text" class="form_subscription_input" name="name" placeholder="Ваше имя">
        <input type="text" class="form_subscription_input" name="mail" placeholder="Ваш email">
        <button class="form_subscription_btn" disabled data-btn-send-js>Подписаться</button>
      </form>
      <label class="start-form_agree-block">
        <input class="basket-modal_checkbox" type="checkbox" data-check-js>
        <span class="basket-modal_custom"></span>
        <span class="basket-modal_text">
          Отправляя свои данные я соглашаюсь с <a href="">политикой конфиденциальности</a> сайта
            и даю согласие на обработку персональных данных
        </span>
      </label>
    </div>
  </div>
</section>
<?php echo $content_top; ?>
<?php echo $footer; ?>