//функция для получения переменных из url
function getQueryVariable(variable) {
  var query = window.location.search.substring(1);
  var vars  = query.split("&");
  for (var i=0;i<vars.length;i++) {
    var pair = vars[i].split("=");
    if(pair[0] == variable){return pair[1];}
  }
  return(false);
}

//получаем цены из url
if(getQueryVariable("minp") && getQueryVariable("maxp")) {
  $('.catalog-price_input_min').val(getQueryVariable("minp"));
  $('.catalog-price_input_max').val(getQueryVariable("maxp"));
}

function filterAndClearFilterClick(baseUrl) {
  $('#button-filter').on('click', function(e, sort) {
    e.preventDefault();
    filter = [];

    $('input[name^=\'filter\']:checked').each(function(element) {
      filter.push(this.value);
    });

    var minPrice = $('.catalog-price_input_min').val();
    var maxPrice = $('.catalog-price_input_max').val();
    if (sort) {
      var sortOrder = sort;
    } else {
      var sortOrder = '';
    }

    if(minPrice != 0 && maxPrice != 0) {
      location = baseUrl+'&filter=' + filter.join(',') + '&minp=' + minPrice + '&maxp=' + maxPrice + sortOrder;
    } else {
      location = baseUrl+'&filter=' + filter.join(',') + sortOrder;
    }
  });

  $('#clear-filter').on('click', function(e) {
    e.preventDefault();

    $('input[name^=\'filter\']:checked').each(function(element) {
      $(this).prop('checked',false);
    });

    location = baseUrl

  });
}

///////////////////Sorting links/////////////////////////
$( document ).ready(function() {
  $('.catalog-base_desktop-sort_item').on('click', function(e) {
    e.preventDefault();
    $('#button-filter').trigger('click',[$(this).find('a').data('sort')]);
  });

  $('#sort-select-mobile').on('change', function(e){
    e.preventDefault();
    $('#button-filter').trigger('click',[$(this).find(':selected').data('sort')]);
  });

  //получаем из url значение текущего sort чтобы оно было выбрано в select по-умолчанию
  $('#sort-select-mobile option[value="'+ getQueryVariable("sort") + '"]').attr('selected','selected');
});

