///////////////////Total price in product page/////////////////////////

$(".attr_mobile_div[data-id-attr='0']").show();
$(".cart-grid_select").on("change", function () {
  var selectedOptionId = $(this).find(':selected').data('id-attr')
  $(".attr_mobile_div").each(function () {
      if ($(this).data('id-attr') == selectedOptionId) {
          $(this).show();
      } else {
          $(this).hide();
      }
  });
});

function getTotalPrice() {
  if ($(".cart-grid_mobile").is(":visible") == true) {
      var totalProductSumMob = 0;
      $(".attr_mobile_div").each(function() {
          totalProductSumMob += $(this).find('span[data-option-price]').data('option-price') * $(this).find('.quantity_mobile').val();
      });				
      $('.cart-etc_total__span').text(totalProductSumMob.toFixed(2));
  } else {
      var totalProductSum = 0;
      $(".cart-table > tbody > tr").each(function() {
          totalProductSum += $(this).find('td[data-option-price]').data('option-price') * $(this).find('.quantity_desktop').val();
      });
      $('.cart-etc_total__span').text(totalProductSum.toFixed(2));
  }			
}

$('.cart-product_count__a').on('click', function(){
getTotalPrice();
})

window.onresize = function(event) {
getTotalPrice();
};

///////////////////Adding quantity in shopping cart/////////////////////////

function getSuccessFromAjax(data) {
  var newFormSum        = $(data).find('.basket-end .basket-end_total').html();
  var newFormContent    = $(data).find('.basket-roster').html();
  var totalProductCount = $(data).find('.cart-product_count-hidden');
  var totalSumm         = 0;
  totalProductCount.each(function(){
    totalSumm += parseInt($(this).val());
  })

  if(!newFormContent) {
    newFormContent = '';
    $('.basket-end').text("Ваша корзина пуста");
  }
  $('.basket-end .basket-end_total').html(newFormSum);
  $('.basket-roster').html(newFormContent);
  $('span.header-basket_count').text(totalSumm);
  $('.basket-wrapper-form [data-dec-js-cart]').click(decProductCount);
  $('.basket-wrapper-form [data-inc-js-cart]').click(incProductCount);
}

function incProductCount() {
  var $input = $(this).parent().find('input');
  $input.val(parseInt($input.val()) + 1);
  $input.change();
  var frm = $('.basket-wrapper-form');
  $.ajax({
      type   : frm.attr('method'),
      url    : frm.attr('action'),
      data   : frm.serialize(),
      success: function (data) {
        getSuccessFromAjax(data);
      }
  }); 
  return false;
}

function decProductCount() {
  var $input = $(this).parent().find('input');
  var count  = parseInt($input.val()) - 1;
      count  = count < 1 ? 0 : count;
  $input.val(count);
  $input.change();
  var frm = $('.basket-wrapper-form');
  $.ajax({
      type   : frm.attr('method'),
      url    : frm.attr('action'),
      data   : frm.serialize(),
      success: function (data) {
        getSuccessFromAjax(data);
      }
  }); 
  return false;
}

$('.basket-wrapper-form [data-dec-js-cart]').click(decProductCount);
$('.basket-wrapper-form [data-inc-js-cart]').click(incProductCount);

///////////////////Changing product thumbs on product page/////////////////////////

$('.cart-more_img .cart-more_link').on('click', function(e) {
  e.preventDefault();
  var bigImage = $(this).parent().parent().parent();
  var curImg   = $(this).find('img').attr('src').replace('74x74','500x500');
  console.log(bigImage);
  console.log(curImg);
  bigImage.find('.cart-product_big-img img').attr('src', curImg);
})

///////////////////AJAX Request in catalog/////////////////////////

$('.catalog-nav_all').on('click', function(e) {
  e.preventDefault();
  var currentLocation = window.location.href;

  //функция для получения переменных из url
  function getQueryVariable(variable) {
    var query = window.location.search.substring(1);
    var vars  = query.split("&");
    for (var i=0;i<vars.length;i++) {
      var pair = vars[i].split("=");
      if(pair[0] == variable){return pair[1];}
    }
    return(false);
  }

  function removeUrlParameter(url, parameter) {
    var urlParts = url.split('?');
  
    if (urlParts.length >= 2) {
      // Get first part, and remove from array
      var urlBase = urlParts.shift();
  
      // Join it back up
      var queryString = urlParts.join('?');
  
      var prefix = encodeURIComponent(parameter) + '=';
      var parts  = queryString.split(/[&;]/g);
  
      // Reverse iteration as may be destructive
      for (var i = parts.length; i-- > 0; ) {
        // Idiom for string.startsWith
        if (parts[i].lastIndexOf(prefix, 0) !== -1) {
          parts.splice(i, 1);
        }
      }
      url = urlBase + '?' + parts.join('&');
    }
    return url;
  }

  if(getQueryVariable('page')) {
    currentLocation = removeUrlParameter(currentLocation, 'page');
  }

  if(currentLocation.indexOf("?") == -1) {
    currentLocation = currentLocation + '?limit=500';
  } else {
    currentLocation = currentLocation + '&limit=500';
  }
  
  $.ajax({
    type   : 'GET',
    url    : currentLocation,
    success: function (data) {
      var newCatalog = $(data).find('.catalog-product').html();
      $('.catalog-product').html(newCatalog);
      $('.catalog-nav').hide();
      $('html, body').animate({
        scrollTop: 0
      }, 'slow');
    }
  }); 
});

///////////////////Orders in account page/////////////////////////
$('.lk-order_body').on('click', function(e) {
  location = '/index.php?route=account/order/info&order_id=' + $(this).find(".order_id_span").text();
});

///////////////////Confirm order in shopping cart/////////////////////////

// $('.confirm_order_btn').on('click', function(e) {

//   e.preventDefault();
//   var frm = $('.basket-modal_form');
//   $.ajax({
//     url       : 'index.php?route=checkout/guest/save',
//     type      : 'post',
//     data      : frm.serialize(),
//     dataType  : 'json',
//     beforeSend: function() {
//     },
//     success: function(json) {

//       $.ajax({
//         url     : 'index.php?route=checkout/confirm',
//         dataType: 'html',
//         complete: function() {
          
//         },
//         success: function(html) {

//           $.ajax({
//             type   : 'get',
//             url    : 'index.php?route=extension/payment/free_checkout/confirm',
//             cache  : false,
//             success: function() {
//               location = 'http://katod.com/index.php?route=checkout/success';
//             }
//           });
          
//         },
//         error: function(xhr, ajaxOptions, thrownError) {
//           alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
//         }
//       });
        
//     },
//     error: function(xhr, ajaxOptions, thrownError) {
//         alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
//     }
// });
  
// })

