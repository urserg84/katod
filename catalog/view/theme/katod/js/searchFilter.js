$( document ).ready(function() {

  //функция для получения переменных из url
  function getQueryVariable(variable) {
    var query = window.location.search.substring(1);
    var vars  = query.split("&");
    for (var i=0;i<vars.length;i++) {
      var pair = vars[i].split("=");
      if(pair[0] == variable){return pair[1];}
    }
    return(false);
  }

  //получаем из url значение текущего sort чтобы оно было выбрано в select по-умолчанию
  $('.catalog-sort_select_search option[value="'+ getQueryVariable("sort") + '"]').attr('selected','selected');

  $('.catalog-base_desktop-sort_item_search').on('click', function(e) {
    e.preventDefault();
    var currentLocation = window.location.href;
    if((currentLocation.split("&").length - 1) <= 1) {
      location = window.location + $(this).find('a').data('sort');
    } else {
      var partsArr = currentLocation.split("&");
          location = partsArr[0] + '&' + partsArr[1] + $(this).find('a').data('sort');
    }      
  });
  
  $('.catalog-sort_select_search').on('change', function(e){
    e.preventDefault();
    var currentLocation = window.location.href;
    if((currentLocation.split("&").length - 1) <= 1) {
      location = window.location + $(this).find(':selected').data('sort');
    } else {
      var partsArr = currentLocation.split("&");
          location = partsArr[0] + '&' + partsArr[1] + $(this).find(':selected').data('sort');
    }     
  });
  
});