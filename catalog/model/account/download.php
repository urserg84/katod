<?php
class ModelAccountDownload extends Model {
	public function getDownload($download_id) {
		$implode = array();

		$order_statuses = $this->config->get('config_complete_status');

		foreach ($order_statuses as $order_status_id) {
			$implode[] = "o.order_status_id = '" . (int)$order_status_id . "'";
		}

		if ($implode) {
			$query = $this->db->query("SELECT d.filename, d.mask FROM `" . DB_PREFIX . "order` o LEFT JOIN " . DB_PREFIX . "order_product op ON (o.order_id = op.order_id) LEFT JOIN " . DB_PREFIX . "product_to_download p2d ON (op.product_id = p2d.product_id) LEFT JOIN " . DB_PREFIX . "download d ON (p2d.download_id = d.download_id) WHERE o.customer_id = '" . (int)$this->customer->getId() . "' AND (" . implode(" OR ", $implode) . ") AND d.download_id = '" . (int)$download_id . "'");

			return $query->row;
		} else {
			return;
		}
	}

	public function getDownloads($start = 0, $limit = 20) {
		if ($start < 0) {
			$start = 0;
		}

		if ($limit < 1) {
			$limit = 20;
		}

		$implode = array();

		$order_statuses = $this->config->get('config_complete_status');

		foreach ($order_statuses as $order_status_id) {
			$implode[] = "o.order_status_id = '" . (int)$order_status_id . "'";
		}

		if ($implode) {
			$query = $this->db->query("SELECT DISTINCT d.download_id, o.order_id, o.date_added, dd.name, d.filename FROM `" . DB_PREFIX . "order` o LEFT JOIN " . DB_PREFIX . "order_product op ON (o.order_id = op.order_id) LEFT JOIN " . DB_PREFIX . "product_to_download p2d ON (op.product_id = p2d.product_id) LEFT JOIN " . DB_PREFIX . "download d ON (p2d.download_id = d.download_id) LEFT JOIN " . DB_PREFIX . "download_description dd ON (d.download_id = dd.download_id) WHERE o.customer_id = '" . (int)$this->customer->getId() . "' AND dd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND (" . implode(" OR ", $implode) . ") ORDER BY o.date_added DESC LIMIT " . (int)$start . "," . (int)$limit);

			return $query->rows;
		} else {
			return array();
		}
	}

	public function getTotalDownloads() {
		$implode = array();

		$order_statuses = $this->config->get('config_complete_status');

		foreach ($order_statuses as $order_status_id) {
			$implode[] = "o.order_status_id = '" . (int)$order_status_id . "'";
		}

		if ($implode) {
			$query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order` o LEFT JOIN " . DB_PREFIX . "order_product op ON (o.order_id = op.order_id) LEFT JOIN " . DB_PREFIX . "product_to_download p2d ON (op.product_id = p2d.product_id) WHERE o.customer_id = '" . (int)$this->customer->getId() . "' AND (" . implode(" OR ", $implode) . ")");

			return $query->row['total'];
		} else {
			return 0;
		}
	}

	public function addDownloadProject($data) {
		$this->db->query("UPDATE `" . DB_PREFIX . "download_project` SET phone = '" . $this->db->escape(utf8_strtolower($data['phone'])) . "', name = '". $this->db->escape(utf8_strtolower($data['name'])) . "', email = '". $this->db->escape(utf8_strtolower($data['email'])) . "', message = '". $this->db->escape(utf8_strtolower($data['message'])) . "', date_added = NOW() WHERE id = '" . (int)$this->db->escape(utf8_strtolower($data['id'])) . "'");
	}	

	public function addDownloadProjectPath($path) {
		$this->db->query("INSERT INTO `" . DB_PREFIX . "download_project` SET file = '". $this->db->escape(utf8_strtolower($path)) . "'");
	}	

	public function getLastProjectId() {
		$query = $this->db->query("SELECT id FROM `" . DB_PREFIX . "download_project` ORDER BY id DESC LIMIT 1");
		return $query->row['id'];
	}
	
	public function getLastProjectPath($id) {
		$query = $this->db->query("SELECT file FROM `" . DB_PREFIX . "download_project` WHERE id = '". (int)$id ."'");
		return $query->row['file'];
	}
}