<?php
class ControllerExtensionModuleBestSeller extends Controller {
	public function index($setting) {
		$this->load->language('extension/module/bestseller');

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_tax'] = $this->language->get('text_tax');

		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');

		$data['products'] = array();
		$this->load->model('catalog/product');

		$product_id = $this->request->get['product_id'];
		$product_cat = $this->model_catalog_product->getCategories($product_id);	
		$product_cat_parent = $this->model_catalog_category->getCategory($product_cat[0]['category_id']);
		$product_category_id = $product_cat[0]['category_id'];

		$results = $this->model_catalog_product->getBestSellerProducts($setting['limit'], $product_category_id);

		if ($results) {
			foreach ($results as $result) {
				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height']);
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
				}

				if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$price = false;
				}

				if ((float)$result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$special = false;
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price'], $this->session->data['currency']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = $result['rating'];
				} else {
					$rating = false;
				}

				$attribute_groups = $this->model_catalog_product->getProductAttributes($result['product_id']);

				//отдельно выводим атрибут в кружке товара в карточке
				$attValue = '';
				if ($attribute_groups) {
					foreach ($attribute_groups as $attribute_group) {
						if ($attribute_group['attribute_group_id'] != "10") {
							continue;
						}
						foreach ($attribute_group['attribute'] as $attribute) {
							if ($attribute['attribute_id'] != "20") {
								continue;
							}
							$attValue = $attribute['text'];
						}
					}
				}

				if(isset($attValue)) {
					$attValue = $attValue;
				} else {
					$attValue = '';
				}
				
				$total_attrubute_clear = [];
				//выводим атрибуты только из 7 группы
				if(!empty($attribute_groups)) {
					foreach ($attribute_groups as $att_group) {
						if ($att_group['attribute_group_id'] == "7") {
							$attribute_groups_clear[] = $att_group;
						}
					}

					//берём только 4 пункта из результата вышестоящей функции
					$i = 0;
					foreach ($attribute_groups_clear as $att_group_clear) {
						foreach($att_group_clear['attribute'] as $attribute_clear) {
							$i++;
							if($i > 4) break;
							$total_attrubute_clear[] = $attribute_clear;	
						}
					}
				}

				$data['products'][] 	= array(
					'product_id'  			=> $result['product_id'],
					'thumb'       			=> $image,
					'name'        			=> $result['name'],
					'attribute_groups'	=> $total_attrubute_clear,
					'description' 			=> utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
					'price'       			=> $price,
					'special'     			=> $special,
					'tax'         			=> $tax,
					'rating'      			=> $rating,
					'href'        			=> $this->url->link('product/product', 'product_id=' . $result['product_id']),
					'top_cyrcle_attr'		=> $attValue
				);
			}

			return $this->load->view('extension/module/bestseller', $data);
		}

	
	}
}
