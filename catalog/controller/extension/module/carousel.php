<?php
class ControllerExtensionModuleCarousel extends Controller {
	public function index($setting) {

		if (isset($this->request->get['route'])) {
			$route = (string)$this->request->get['route'];
		} else {
			$route = 'common/home';
		}
		
		static $module = 0;

		$this->load->model('design/banner');
		$this->load->model('tool/image');

		

		$data['banners'] = array();

		$results = $this->model_design_banner->getBanner($setting['banner_id']);

		foreach ($results as $result) {
			if (is_file(DIR_IMAGE . $result['image'])) {
				$data['banners'][] = array(
					'title' => $result['title'],
					'link'  => $result['link'],
					'image' => $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height'])
				);
			}
		}

		$data['module'] = $module++;
		$data['route'] = $route;

		return $this->load->view('extension/module/carousel', $data);
	}
}