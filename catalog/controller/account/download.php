<?php
class ControllerAccountDownload extends Controller {
	public function index() {
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/download', '', true);

			$this->response->redirect($this->url->link('account/login', '', true));
		}

		$this->load->language('account/download');

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_downloads'),
			'href' => $this->url->link('account/download', '', true)
		);

		$this->load->model('account/download');

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_empty'] = $this->language->get('text_empty');

		$data['column_order_id'] = $this->language->get('column_order_id');
		$data['column_name'] = $this->language->get('column_name');
		$data['column_size'] = $this->language->get('column_size');
		$data['column_date_added'] = $this->language->get('column_date_added');

		$data['button_download'] = $this->language->get('button_download');
		$data['button_continue'] = $this->language->get('button_continue');

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$data['downloads'] = array();

		$download_total = $this->model_account_download->getTotalDownloads();

		$results = $this->model_account_download->getDownloads(($page - 1) * $this->config->get($this->config->get('config_theme') . '_product_limit'), $this->config->get($this->config->get('config_theme') . '_product_limit'));

		foreach ($results as $result) {
			if (file_exists(DIR_DOWNLOAD . $result['filename'])) {
				$size = filesize(DIR_DOWNLOAD . $result['filename']);

				$i = 0;

				$suffix = array(
					'B',
					'KB',
					'MB',
					'GB',
					'TB',
					'PB',
					'EB',
					'ZB',
					'YB'
				);

				while (($size / 1024) > 1) {
					$size = $size / 1024;
					$i++;
				}

				$data['downloads'][] = array(
					'order_id'   => $result['order_id'],
					'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
					'name'       => $result['name'],
					'size'       => round(substr($size, 0, strpos($size, '.') + 4), 2) . $suffix[$i],
					'href'       => $this->url->link('account/download/download', 'download_id=' . $result['download_id'], true)
				);
			}
		}

		$pagination = new Pagination();
		$pagination->total = $download_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get($this->config->get('config_theme') . '_product_limit');
		$pagination->url = $this->url->link('account/download', 'page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($download_total) ? (($page - 1) * $this->config->get($this->config->get('config_theme') . '_product_limit')) + 1 : 0, ((($page - 1) * $this->config->get($this->config->get('config_theme') . '_product_limit')) > ($download_total - $this->config->get($this->config->get('config_theme') . '_product_limit'))) ? $download_total : ((($page - 1) * $this->config->get($this->config->get('config_theme') . '_product_limit')) + $this->config->get($this->config->get($this->config->get('config_theme') . '_theme') . '_product_limit')), $download_total, ceil($download_total / $this->config->get($this->config->get('config_theme') . '_product_limit')));

		$data['continue'] = $this->url->link('account/account', '', true);

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('account/download', $data));
	}

	public function download() {
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/download', '', true);

			$this->response->redirect($this->url->link('account/login', '', true));
		}

		$this->load->model('account/download');

		if (isset($this->request->get['download_id'])) {
			$download_id = $this->request->get['download_id'];
		} else {
			$download_id = 0;
		}

		$download_info = $this->model_account_download->getDownload($download_id);

		if ($download_info) {
			$file = DIR_DOWNLOAD . $download_info['filename'];
			$mask = basename($download_info['mask']);

			if (!headers_sent()) {
				if (file_exists($file)) {
					header('Content-Type: application/octet-stream');
					header('Content-Disposition: attachment; filename="' . ($mask ? $mask : basename($file)) . '"');
					header('Expires: 0');
					header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
					header('Pragma: public');
					header('Content-Length: ' . filesize($file));

					if (ob_get_level()) {
						ob_end_clean();
					}

					readfile($file, 'rb');

					exit();
				} else {
					exit('Error: Could not find file ' . $file . '!');
				}
			} else {
				exit('Error: Headers already sent out!');
			}
		} else {
			$this->response->redirect($this->url->link('account/download', '', true));
		}
	}

	public function downloadProjectData(){
		$this->load->model('account/download');

		if(isset($this->request->post['name'])) {
			$name = filter_var($this->request->post['name'], FILTER_SANITIZE_STRING);
			$data['name'] = $name;
		} else {
			$name = '';
		}

		if(isset($this->request->post['mail'])) {
			$email = filter_var($this->request->post['mail'], FILTER_SANITIZE_STRING);
			$data['email'] = $email;
		} else {
			$email = '';
		}

		if(isset($this->request->post['phone'])) {
			$phone = filter_var($this->request->post['phone'], FILTER_SANITIZE_STRING);
			$data['phone'] = $phone;
		} else {
			$phone = '';
		}

		if(isset($this->request->post['message'])) {
			$message = filter_var($this->request->post['message'], FILTER_SANITIZE_STRING);
			$data['message'] = $message;
		} else {
			$message = '';
		}

		if(isset($this->request->post['id'])) {
			$id = filter_var($this->request->post['id'], FILTER_SANITIZE_STRING);
		} else {
			$id = 0;
		}

		$downloadData = [
			'name' => $name,
			'email' => $email,
			'phone' => $phone,
			'message' => $message,
			'id' => $id
		];

		$this->model_account_download->addDownloadProject($downloadData);
		$path = $this->model_account_download->getLastProjectPath($id);
		
		if(isset($path)) {
			$data['path'] = $path;
		} 

		$mail = new Mail();
		$mail->protocol = $this->config->get('config_mail_protocol');
		$mail->parameter = $this->config->get('config_mail_parameter');
		$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
		$mail->smtp_username = $this->config->get('config_mail_smtp_username');
		$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
		$mail->smtp_port = $this->config->get('config_mail_smtp_port');
		$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

		$mail->setFrom($this->config->get('config_email'));
		$mail->setSender(html_entity_decode("Диод", ENT_QUOTES, 'UTF-8'));
		$mail->setSubject(html_entity_decode("Получение данных от клиента", ENT_QUOTES, 'UTF-8'));
		$mail->setHtml($this->load->view('mail/project', $data));

		$emails = explode(',', $this->config->get('config_mail_alert_email'));
	
		foreach ($emails as $emailitem) {
			if ($emailitem && filter_var($emailitem, FILTER_VALIDATE_EMAIL)) {
				$mail->setTo($emailitem);
				$mail->send();
			}
		}
	}

	public function downloadProjectFile(){
		$this->load->model('account/download');

		if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
				$base_url = $this->config->get('config_ssl');
		} else {
			$base_url = $this->config->get('config_url');
		}

		$data = array();
		$error = false;
		$files = array();
		$uploaddir = './image/catalog/downloads/';
		$max_file_size = 10485760;
		$rand = rand(0000,9999).time();

		if(isset($_FILES['file']))
		{	
			$file = $_FILES['file'];
			if($file['size'] < $max_file_size) 
			{
				if(move_uploaded_file($file['tmp_name'], $uploaddir .basename($rand.$file['name'])))
				{
					$files = $base_url .$uploaddir .$rand.$file['name'];

					$this->model_account_download->addDownloadProjectPath($files);
					$lastId = $this->model_account_download->getLastProjectId();
				}
				else
				{
					$error = true;
				}
			}
		} else {
			$lastId = 0;
		}

		$data = ($error) ? array('error' => 'There was an error uploading your files') : array('id' => $lastId);
		
		echo json_encode($data);
	}
}