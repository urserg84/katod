$('[data-toggle-js]').click(function(){

  if($(this).hasClass('header-mobile-toggle__active')){

    $('[data-toggle-js]').removeClass('header-mobile-toggle__active');
    $('.header_content__nav').removeClass('header_content__nav__active');

    $('body').removeClass('fixed');

  }else{

    $('[data-toggle-js]').addClass('header-mobile-toggle__active');
    $('.header_content__nav').addClass('header_content__nav__active');

    $('body').addClass('fixed');
  }
});

$('.slider').owlCarousel({
  items     : 1,
  dots      : true,
  mouseDrag : false,
  nav       : true,
  navElement: 'div',
  navText   : ["<svg width='9' height='15' viewBox='0 0 9 15' fill='none' xmlns='http://www.w3.org/2000/svg'><path d='M6.2 0L0 6.2L6.2 12.5' transform='translate(1.82532 0.98114)' stroke='#2A2D33' stroke-width='2'/></svg>", "<svg width='9' height='15' viewBox='0 0 9 15' fill='none' xmlns='http://www.w3.org/2000/svg'><path d='M6.2 0L0 6.2L6.2 12.5' transform='translate(7.39697 0.98114) scale(-1 1)' stroke='#2A2D33' stroke-width='2'/></svg>"],
});

$('.carousel-slider').owlCarousel({
  nav            : true,
  responsiveClass: true,
  mouseDrag      : false,
  navText        : ["<svg width='13' height='9' viewBox='0 0 13 9' fill='none' xmlns='http://www.w3.org/2000/svg'><path d='M11.0641 0.353553C11.2594 0.158291 11.2594 -0.158291 11.0641 -0.353553L7.88214 -3.53553C7.68688 -3.7308 7.3703 -3.7308 7.17504 -3.53553C6.97978 -3.34027 6.97978 -3.02369 7.17504 -2.82843L10.0035 0L7.17504 2.82843C6.97978 3.02369 6.97978 3.34027 7.17504 3.53553C7.3703 3.7308 7.68688 3.7308 7.88214 3.53553L11.0641 0.353553ZM0 0.5H10.7106V-0.5H0V0.5Z' transform='translate(12.1567 4.34167) scale(-1 1)' fill='#8C8C8C'/></svg>", "<svg width='13' height='9' viewBox='0 0 13 9' fill='none' xmlns='http://www.w3.org/2000/svg'> <path d='M11.0641 0.353553C11.2594 0.158291 11.2594 -0.158291 11.0641 -0.353553L7.88214 -3.53553C7.68688 -3.7308 7.3703 -3.7308 7.17504 -3.53553C6.97978 -3.34027 6.97978 -3.02369 7.17504 -2.82843L10.0035 0L7.17504 2.82843C6.97978 3.02369 6.97978 3.34027 7.17504 3.53553C7.3703 3.7308 7.68688 3.7308 7.88214 3.53553L11.0641 0.353553ZM0 0.5H10.7106V-0.5H0V0.5Z' transform='translate(0.843262 4.34167)' fill='#8C8C8C'/></svg>"],
  navElement     : 'div',
  responsive     : {
        0: {

            items: 1
        },
        1240: {

            items : 4,
            margin: 20
        }
    }
});

var articleCarousel = $('.article-slide').owlCarousel({
  items     : 1,
  nav       : true,
  navElement: 'div',
  navText   : ["<svg width='9' height='15' viewBox='0 0 9 15' fill='none' xmlns='http://www.w3.org/2000/svg'><path d='M6.2 0L0 6.2L6.2 12.5' transform='translate(1.82532 0.98114)' stroke='#ffffff' stroke-width='2'/></svg>", "<svg width='9' height='15' viewBox='0 0 9 15' fill='none' xmlns='http://www.w3.org/2000/svg'><path d='M6.2 0L0 6.2L6.2 12.5' transform='translate(7.39697 0.98114) scale(-1 1)' stroke='#ffffff' stroke-width='2'/></svg>"]
})

if($(window).width() >= 1240) {

    articleCarousel.trigger('destroy.owl.carousel');
}

$(window).on('resize', function() {

    if($(window).width() >= 1240) {

        articleCarousel.trigger('destroy.owl.carousel');
    }
});

$('[data-show-filter-js]').click(function(e){
  e.preventDefault();

  $('.catalog-filter_form-block').toggleClass('catalog-filter_form-block__active');
})

$('[data-hide-filter-js]').click(function(){

  $('.catalog-filter_form-block').removeClass('catalog-filter_form-block__active');
})

$('.product-more_carousel').owlCarousel({
  nav            : true,
  responsiveClass: true,
  mouseDrag      : false,
  navElement     : 'div',
  navText        : ["<svg width='9' height='15' viewBox='0 0 9 15' fill='none' xmlns='http://www.w3.org/2000/svg'><path d='M6.2 0L0 6.2L6.2 12.5' transform='translate(1.82532 0.98114)' stroke='#000000' stroke-width='2'/></svg>", "<svg width='9' height='15' viewBox='0 0 9 15' fill='none' xmlns='http://www.w3.org/2000/svg'><path d='M6.2 0L0 6.2L6.2 12.5' transform='translate(7.39697 0.98114) scale(-1 1)' stroke='#000000' stroke-width='2'/></svg>"],
  responsive     : {
        0: {

            items: 3
        },
        1240: {

            items : 8,
            margin: 20
        }
    }
})

$('.cart-slider').owlCarousel({
  items     : 1,
  nav       : true,
  navElement: 'div',
  navText   : ["<svg width='9' height='15' viewBox='0 0 9 15' fill='none' xmlns='http://www.w3.org/2000/svg'><path d='M6.2 0L0 6.2L6.2 12.5' transform='translate(1.82532 0.98114)' stroke='#2A2D33' stroke-width='2'/></svg>", "<svg width='9' height='15' viewBox='0 0 9 15' fill='none' xmlns='http://www.w3.org/2000/svg'><path d='M6.2 0L0 6.2L6.2 12.5' transform='translate(7.39697 0.98114) scale(-1 1)' stroke='#2A2D33' stroke-width='2'/></svg>"]
});

$('[data-show-char-js]').click(function(e){
  e.preventDefault();

  $('.cart-characteristic_wrap').toggle();
})

$('[data-hide-char-js]').click(function(e){
  e.preventDefault();

  $('.cart-characteristic_wrap').toggle();
})

$('[data-dec-js]').click(function () {

  var $input = $(this).parent().find('input');
  var count  = parseInt($input.val()) - 1;
      count  = count < 1 ? 0 : count;
  $input.val(count);
  $input.change();
  return false;
});

$('[data-inc-js]').click(function () {

  var $input = $(this).parent().find('input');
  $input.val(parseInt($input.val()) + 1);
  $input.change();
  return false;
});

$('[data-basket-order-js]').click(function(e){

    e.preventDefault();

    $('.overlay').fadeIn(function(){

        $('.basket-modal').fadeIn();

        $('.overlay, .basket-modal').bind('mousewheel touchmove', function(e){

            e.preventDefault();
        })
    })
});

$('.overlay, .basket-modal_exit').click(function(){

    $('.basket-modal').fadeOut(function(){

        $('.overlay').fadeOut();
    });
});

$(window).scroll(function(){

    let thisScroll = $(this).scrollTop();

    if($(window).width() > 1239) {

        if(thisScroll > $('.header').height()) {

            $('.header').addClass('header-fixed');

            setTimeout(function(){
                $('.header').addClass('header-fixed__show');
            }, 100);

            $('.header-logo_img-block, .header-logo_img-title, .header-desktop_city-phone, .header-upload_block, .header-interaction_basket-block').removeClass('fadeInUp');
        }else{

            $('.header').removeClass('header-fixed header-fixed__show');
        }
    }
});

$('[data-check-js]').change(function() {

    var thisCheck = $(this);

    if($(this).prop('checked')) {

        thisCheck.closest('[data-parent-form-js]').find('[data-btn-send-js], [data-modal-send-js]').removeAttr('disabled');

    }else{

        thisCheck.closest('[data-parent-form-js]').find('[data-btn-send-js], [data-modal-send-js]').attr('disabled', true)
    }
});

$('.catalog-check_input').change(function(){

    if($(this).prop('checked')){

        $(this).parent().find('.catalog-check_name').css('color', '#FFA92E');
    }else{

        $(this).parent().find('.catalog-check_name').css('color', '#2A2D33');
    }
})

function validatePageForm(name, mail, phone, that) {

    let error  = false;
    let filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,6})+$/;

    if($(name).length > 0 && $(mail).length > 0) {

        if($(name).val() !== '' && $(name).val().length > 2) {

            $(name).removeClass('form_subscription_input__error')
        }else{

            $(name).addClass('form_subscription_input__error');

            error = true
        }

        if (filter.test($(mail).val())) {

            $(mail).removeClass('form_subscription_input__error')
        } else {

            $(mail).addClass('form_subscription_input__error')

            error = true;
        }

        if(!error) {

            $('.form_subscription__success').fadeIn(function(){


                $(that).closest('.form_subscription__wrapper').find('[data-check-js]').prop('checked', false);
                $(that).closest('.form_subscription__wrapper').find('[data-btn-send-js]').attr('disabled', true);

                $(that).find('div').html('<p>Вы подписались</p>')

                setTimeout(() => {

                    $('.form_subscription__success').fadeOut();
                    $(name).val('')
                    $(mail).val('')

                }, 5000);
            });

            return true;
        }
    }


    if($(name).length > 0 && $(phone).length > 0) {

        if($(name).val() !== '' && $(name).val().length > 2) {

            $(name).removeClass('form_subscription_input__error')
        }else{

            $(name).addClass('form_subscription_input__error');

            error = true
        }

        if($(phone).val() !== '' && $(phone).val().length > 6) {

            $(phone).removeClass('form_subscription_input__error')
        }else{

            $(phone).addClass('form_subscription_input__error');

            error = true
        }

        if(!error) {

            console.log('name and phone')

            $('.form_subscription__success').fadeIn(function(){

                $(that).closest('.form_subscription__wrapper').find('[data-check-js]').prop('checked', false);
                $(that).closest('.form_subscription__wrapper').find('[data-btn-send-js]').attr('disabled', true);

                $(that).find('div').html('<p>Ваше сообщение отправлено</p>');

                setTimeout(() => {

                    $('.form_subscription__success').fadeOut();
                    $(name).val('')
                    $(phone).val('')

                }, 5000);
            });

            return true;
        }
    }
}

$('[data-submit-form-js]').submit(function(e){

    e.preventDefault();

    let form = $(this);

    let name  = form.find('input[name="name"]');
    let mail  = form.find('input[name="mail"]');
    let phone = form.find('input[name="phone"]');

    if(validatePageForm(name, mail, phone, this) === true){

        let nameAff  = typeof name === "undefined" ? '' : name.val();
        let mailAff  = typeof mail === "undefined" ? '' : mail.val();
        let phoneAff = typeof phone === "undefined" ? '' : phone.val();

        $.ajax({
            type   : 'POST',
            url    : 'index.php?route=affiliate/account/addSubscriber',
            data   : {name:nameAff, mail:mailAff, phone:phoneAff},
            success: function (data) {
                
            }
        }); 
    }
});

$('[data-project-form-js]').click(function(e){

    e.preventDefault();

    $('.overlay').fadeIn(function(){

        $('.modal-project').fadeIn()
    });

    $('.page-wrap').addClass('page-wrap_blur')
})

$('.modal-project_exit, .overlay').click(function(){

    $('.modal-project').fadeOut(function(){

        $('.overlay').fadeOut();
        $('.page-wrap').removeClass('page-wrap_blur');
        $('.modal-project__success').fadeOut();

        $('.modal-project_form input, .modal-project_form textarea').val('');
    });
});


$('.header-nav_link').click(function(e){

    e.preventDefault();

    if($(window).width() < 1240) {

        $(this).next('.header-nav_content').toggle();
    }
});

$('#downloadProject').on('change', prepareUpload);

var files;

function prepareUpload(event) {
    files = event.target.files;
    console.log(files);
}

function submitForm(event, data) {

        $form    = $(event.target);
    var formData = $form.serialize();
        $file    = JSON.parse(data).files[0];

    formData = formData + '&file=' + $file;

    $.ajax({
        url  : 'index.php?route=account/download/downloadProjectData',
        type : 'POST',
        data : formData,
        cache: false
    });
}

function uploadFiles(event) {
    event.stopPropagation();
    event.preventDefault();

    var dataForm = new FormData();

    dataForm.append('file', files[0]);

    $.ajax({
        url        : 'index.php?route=account/download/downloadProjectFile',
        type       : 'POST',
        data       : dataForm,
        cache      : false,
        processData: false,
        contentType: false,
        success    : function(data, textStatus, jqXHR)
        {
            if(typeof data.error === 'undefined')
            {
                submitForm(event, data);
            }
            else
            {
                console.log('ERRORS: ' + data.error);
            }
        }
    });
}

$('[data-send-project-js]').submit(function (e) {
    e.preventDefault();

    let error        = false,
        nameVal      = $('.modal-project_input[name="name"]').val(),
        mailVal      = $('.modal-project_input[name="mail"]').val(),
        phoneVal     = $('.modal-project_input[name="phone"]').val(),
        messageVal   = $('.modal-project_textarea').val(),
        phone_filter = /^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){10,14}(\s*)?$/,
        filter       = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,6})+$/;

    if(nameVal !== '') {

        $('.modal-project_input[name="name"]').removeClass('form_subscription_input__error')
    } else {

        $('.modal-project_input[name="name"]').addClass('form_subscription_input__error')

        error = true;
    }

    if (filter.test(mailVal)) {

        $('.modal-project_input[name="mail"]').removeClass('form_subscription_input__error')

    } else {

        error = true;

        $('.modal-project_input[name="mail"]').addClass('form_subscription_input__error')
    }

    if (phone_filter.test(phoneVal)) {

        $('.modal-project_input[name="phone"]').removeClass('form_subscription_input__error');

    } else {

        error = true;

        $('.modal-project_input[name="phone"]').addClass('form_subscription_input__error');
    }

    if(!error) {

        uploadFiles(e);

        $('.modal-project__success').fadeIn();
    }
});

$('[data-price-min-js], [data-price-max-js]').on('keyup', function(){

    var n = parseInt($(this).val().replace(/\D/g,''),10);
    $(this).val(n.toLocaleString());
});
