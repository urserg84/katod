<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <?php if ($attention) { ?>
  <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $attention; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <?php if ($error_warning) { ?>
  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1><?php echo $heading_title; ?>
        <?php if ($weight) { ?>
        &nbsp;(<?php echo $weight; ?>)
        <?php } ?>
      </h1>
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
        <div class="table-responsive">
          <table class="table table-bordered">
            <thead>
              <tr>
                <td class="text-center"><?php echo $column_image; ?></td>
                <td class="text-left"><?php echo $column_name; ?></td>
                <td class="text-left"><?php echo $column_model; ?></td>
                <td class="text-left"><?php echo $column_quantity; ?></td>
                <td class="text-right"><?php echo $column_price; ?></td>
                <td class="text-right"><?php echo $column_total; ?></td>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($products as $product) { ?>
              <tr>
                <td class="text-center"><?php if ($product['thumb']) { ?>
                  <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-thumbnail" /></a>
                  <?php } ?></td>
                <td class="text-left"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                  <?php if (!$product['stock']) { ?>
                  <span class="text-danger">***</span>
                  <?php } ?>
                  <?php if ($product['option']) { ?>
                  <?php foreach ($product['option'] as $option) { ?>
                  <br />
                  <small><?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
                  <?php } ?>
                  <?php } ?>
                  <?php if ($product['reward']) { ?>
                  <br />
                  <small><?php echo $product['reward']; ?></small>
                  <?php } ?>
                  <?php if ($product['recurring']) { ?>
                  <br />
                  <span class="label label-info"><?php echo $text_recurring_item; ?></span> <small><?php echo $product['recurring']; ?></small>
                  <?php } ?></td>
                <td class="text-left"><?php echo $product['model']; ?></td>
                <td class="text-left"><div class="input-group btn-block" style="max-width: 200px;">
                    <input type="text" name="quantity[<?php echo $product['cart_id']; ?>]" value="<?php echo $product['quantity']; ?>" size="1" class="form-control" />
                    <span class="input-group-btn">
                    <button type="submit" data-toggle="tooltip" title="<?php echo $button_update; ?>" class="btn btn-primary"><i class="fa fa-refresh"></i></button>
                    <button type="button" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger" onclick="cart.remove('<?php echo $product['cart_id']; ?>');"><i class="fa fa-times-circle"></i></button>
                    </span></div></td>
                <td class="text-right"><?php echo $product['price']; ?></td>
                <td class="text-right"><?php echo $product['total']; ?></td>
              </tr>
              <?php } ?>
              <?php foreach ($vouchers as $voucher) { ?>
              <tr>
                <td></td>
                <td class="text-left"><?php echo $voucher['description']; ?></td>
                <td class="text-left"></td>
                <td class="text-left"><div class="input-group btn-block" style="max-width: 200px;">
                    <input type="text" name="" value="1" size="1" disabled="disabled" class="form-control" />
                    <span class="input-group-btn">
                    <button type="button" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger" onclick="voucher.remove('<?php echo $voucher['key']; ?>');"><i class="fa fa-times-circle"></i></button>
                    </span></div></td>
                <td class="text-right"><?php echo $voucher['amount']; ?></td>
                <td class="text-right"><?php echo $voucher['amount']; ?></td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </form>
      <?php if ($modules) { ?>
      <h2><?php echo $text_next; ?></h2>
      <p><?php echo $text_next_choice; ?></p>
      <div class="panel-group" id="accordion">
        <?php foreach ($modules as $module) { ?>
        <?php echo $module; ?>
        <?php } ?>
      </div>
      <?php } ?>
      <br />
      <div class="row">
        <div class="col-sm-4 col-sm-offset-8">
          <table class="table table-bordered">
            <?php foreach ($totals as $total) { ?>
            <tr>
              <td class="text-right"><strong><?php echo $total['title']; ?>:</strong></td>
              <td class="text-right"><?php echo $total['text']; ?></td>
            </tr>
            <?php } ?>
          </table>
        </div>
      </div>
      <div class="buttons clearfix">
        <div class="pull-left"><a href="<?php echo $continue; ?>" class="btn btn-default"><?php echo $button_shopping; ?></a></div>
        <div class="pull-right"><a href="<?php echo $checkout; ?>" class="btn btn-primary"><?php echo $button_checkout; ?></a></div>
      </div>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>


<?php echo $header; ?>
<section class="basket">
    <div class="basket_content">
      <div class="article-page_breadcrumbs">
        <ul itemscope="" itemtype="http://schema.org/BreadcrumbList" class="article-breadcrumbs_list">
          <?php foreach ($breadcrumbs as $key => $breadcrumb) { ?>
            <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem" class="article-breadcrumbs_item">
                <a itemprop="item" href="<?php echo $breadcrumb['href']; ?>" class="article-breadcrumbs_link"><span itemprop="name"><?php echo $breadcrumb['text']; ?></span></a>
                <meta itemprop="position" content="<?=$key ?>">
            </li>
          <?php } ?>
        </ul>
      </div>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
      <div class="basket_head">
        <p class="basket_title">Ваша корзина</p>
        <p class="basket_text">Внимание! Доставка оплачивается отдельно.</p>
      </div>
      <div class="basket-wrapper">
        <div class="basket-roster">
          <div class="basket-roster_head">
            <span class="basket-head_name">Наименование товара</span>
            <span class="basket-head_price">Цена</span>
            <span class="basket-head_count">Количество</span>
            <span class="basket-head_total-price">Сумма</span>
          </div>
          <div class="basket-roster_item">
            <div class="basket-name">
              <div class="basket-name_content">
                <div class="basket-name_img">
                  <img src="dist/image/carousel.png" alt="">
                </div>
                <div class="basket-name_wrap">
                  <p class="basket-name_title">Светодиодный светильник HC-GL638L накладной</p>
                  <p class="basket-name_property">3000 K</p>
                  <p class="basket-name_property">Неконтролируемый</p>
                  <p class="basket-name_property">Код товара: GU10/7WCOB</p>
                </div>
              </div>
            </div>
            <div class="basket-price">
              <span>255p.</span>
            </div>
            <div class="basket-count">
              <span class="cart-grid_property">
                <a href="" class="cart-product_count__dec" data-dec-js>
                  <svg width="18" height="3" viewBox="0 0 18 3" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M17.5 0V1.5H0V0H17.5Z" transform="translate(0.0161133 0.96582)" fill="#FF9500"/>
                  </svg>
                </a>
                <input type="text" class="cart-product_count" readonly="readonly" value="0" readonly>
                <a href="" class="cart-product_count__inc" data-inc-js>
                  <svg width="18" height="19" viewBox="0 0 18 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M9.5 0H8V8H0V9.5H8V17.5H9.5V9.5H17.5V8H9.5V0Z" transform="translate(0.0161133 0.96582)" fill="#FF9500"/>
                  </svg>
                </a>
              </span>
            </div>
            <div class="basket-total-price">
              <span>255p.</span>
              <a href="" class="basket-item_delete"></a>
            </div>
          </div>
          <div class="basket-roster_item">
            <div class="basket-name">
              <div class="basket-name_content">
                <div class="basket-name_img">
                  <img src="dist/image/carousel.png" alt="">
                </div>
                <div class="basket-name_wrap">
                  <p class="basket-name_title">Светодиодный светильник HC-GL638L накладной</p>
                  <p class="basket-name_property">3000 K</p>
                  <p class="basket-name_property">Неконтролируемый</p>
                  <p class="basket-name_property">Код товара: GU10/7WCOB</p>
                </div>
              </div>
            </div>
            <div class="basket-price">
              <span>255p.</span>
            </div>
            <div class="basket-count">
              <span class="cart-grid_property">
                <a href="" class="cart-product_count__dec" data-dec-js>
                  <svg width="18" height="3" viewBox="0 0 18 3" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M17.5 0V1.5H0V0H17.5Z" transform="translate(0.0161133 0.96582)" fill="#FF9500"/>
                  </svg>
                </a>
                <input type="text" class="cart-product_count" readonly="readonly" value="0" readonly>
                <a href="" class="cart-product_count__inc" data-inc-js>
                  <svg width="18" height="19" viewBox="0 0 18 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M9.5 0H8V8H0V9.5H8V17.5H9.5V9.5H17.5V8H9.5V0Z" transform="translate(0.0161133 0.96582)" fill="#FF9500"/>
                  </svg>
                </a>
              </span>
            </div>
            <div class="basket-total-price">
              <span>255p.</span>
              <a href="" class="basket-item_delete"></a>
            </div>
          </div>
        </div>
        <div class="basket-end">
          <div class="basket-end_desktop">
            <p class="basket-end_total">Итого: 28 416 руб.</p>
            <a href="" class="basket-end_btn-continue  basket-end_btn-continue__desktop">Продолжить покупки</a>
          </div>
          <div class="basket-end_btn">
            <a href="" class="basket-end_btn-order" data-basket-order-js>Оформить заказ</a>
          </div>
        </div>
      </div>
    </div>
  </section>
<?php echo $content_bottom; ?>
<?php echo $column_right; ?>
<?php echo $footer; ?>